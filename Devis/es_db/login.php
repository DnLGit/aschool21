<?php

    /*  https://tryphp.w3schools.com/showphpfile.php?filename=demo_db_select_proc
        GUARDA QUA PER AVERE CODICE GENERALE DI UNA SELECT DA PHP*/

    if(!isset($_POST["username"]) || !isset($_POST["password"]))
    {
        header("location:index.php?error=login");
        die();
    }

    $user = $_POST["username"];
    $pwd = $_POST["password"];

    $pwd = hash('sha256', $pwd);

    $conn = mysqli_connect("localhost","DnL","B32","db_es_devis");//conn server to dbms
    $query = "SELECT username, password, nome, cognome, anno_nascita FROM utenti WHERE username = '$user' AND password = '$pwd'";
    
    // la risposta alla chiamata mysqli_query, in caso venga fatta una select, contiene le righe selezionate 
    // quindi non più un solo true/false (anca perché qua no gà senso!!)
    $result = mysqli_query($conn, $query);//conn + query -> server richiede dbms di eseguire $query

    //mysqli_num_rows(ris) dove ris è risultato della SELECT ritorna il numero di righe fetchate
    if (mysqli_num_rows($result) == 1) 
    {
        //login avvenuto con successo
        //login andato a buon fine -> creo una sessione
        session_start();

        //metti dentro a $row la riga fricevuta in risposta
        $row = mysqli_fetch_assoc($result); 
        
        //salvo nelle variabili di sessione i dati dell'utente contenuti nella riga appena ricevuta dal db in risposta alla SELECT
        $_SESSION['username'] = $row['username'];
        $_SESSION['nome'] = $row['nome'];
        $_SESSION['cognome'] = $row['cognome'];
        $_SESSION['anno_nascita'] = $row['anno_nascita'];
        

        header("location:home.php");

    } 
    else 
    {   //login errato perché la SELECT non ha trovato un matching (una riga con quell'username e quella hash)
        // echo "Login errore! <a href=\"index.php\">Riprova</a>"; //puoi fare anche così (SENZA REDIRECT AUTOMATICO!!)
        header("location:index.php?error=login");
    }

    mysqli_close($conn);