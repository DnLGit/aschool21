<?php 
    
    session_start();//carico i dati sull'array $_SESSION sulla base del cookie sess_id arrivato


    if(!isset($_SESSION['username']))  //se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }  

    /*se username presente nelle variabili di sessione, 
    è per forza la sua (non si discute!) e quindi significa che si è autenticato correttamente */
?>
<!DOCTYPE html>
<html>
    <head>
        <title> DevisPlatform </title>
        <meta charset="UTF-8"/>
    </head>
    <body>
        <h1>HOME</h1>
        <div>

            <?php 
                /*
                    NOTA BENE
                    scrivere <?php echo $_SESSION['n']; ?>
                    è equivalente a <?=$_SESSION['n']?>

                */
            ?>

            Benvenuto <?=$_SESSION['username']?>
            
            <?=$_SESSION['nome']." ".$_SESSION['cognome']?> 
            Nato il <?=$_SESSION['anno_nascita']?>
        </div>

        <hr/>
        <a href="logout.php">Logout</a>
    </body>
</html>