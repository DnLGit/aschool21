<?php

    if( !isset($_POST["username"]) || !isset($_POST["password1"]) || !isset($_POST["password2"]) ||
        !isset($_POST["name"]) || !isset($_POST["surname"]) || !isset($_POST["year"]))
    {
        header("location:index.php?error=register");
        die();
    }

    
    $user = $_POST["username"];
    $pwd1 = $_POST["password1"];
    $pwd2 = $_POST["password2"];
    $name = $_POST["name"];
    $surname = $_POST["surname"];
    $year = $_POST["year"];

    //aggiungere altri controlli per validare input (es. nome utente almeno 3 caratteri)

    if($pwd1 != $pwd2)
    {//password non corrispondono
        header("location:index.php?error=register&pwd=different");
        die();
    }

    $pwd_h = hash('sha256', $pwd1);

    /*  SERVER SI CONNETTE AL DBMS
        dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente 'DnL' e pwd 'B32'           [creato uguale su Pc-HP7]
        server si connette al db 'db_es_devis'
    */
    $conn = mysqli_connect("localhost","DnL","B32","db_es_devis");//conn server to dbms
    
    /*Query da inviare al dbms perché la esegua (voglio che inserisca la riga del nuovo utente che si sta registrando)*/
    $query = "INSERT INTO utenti(username, password, nome, cognome, anno_nascita) VALUES('$user', '$pwd_h', '$name', '$surname',$year)";

    /*Uso mysqli_query per inviare la query al db, 
    dove il primo parametro (qui $conn) della chiamata è una connessione già creata e 
    il secondo parametro (qui $query) il comando che si vuole il dbms esegua
    
    in risposta true/false ad indicare l'esito dell'operazione
    */
    $result = mysqli_query($conn, $query);//conn + query -> server richiede dbms di eseguire $query
    
    if ($result)//dbms risponde con TRUE se operazione andata a buon fine
        header("location:index.php?register=success");
    else 
    {
        header("location:index.php?error=register");
    }

    /*CHIUDO la connessione tra db e server (per questioni di memoria)*/ 
    mysqli_close($conn);