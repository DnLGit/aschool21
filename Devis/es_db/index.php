<!DOCTYPE html>
<html>
    <head>
        <title> DevisPlatform </title>
        <meta charset="UTF-8"/>
    </head>
    <body>
        <div>
            <h1> LOGIN</h1>
            <hr><br/>
            <form method="POST" action="login.php">     
                Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>        
                Password: <input type="password" name="password" placeholder="Password" required/><br/>
                
                <input type="submit" name="submit" value="Invia"/>
                <input type="reset" name="cancella" value="Cancella Tutto"/>
            </form>
            <?php 
                if(isset($_GET['error']) && $_GET['error']=='login')
                    echo "<p style='color: red'>ERRORE IN LOGIN</p>";
            ?>
        </div>
        <hr/>
        <div style="margin-top: 5%;">
            <h1> REGISTER</h1>
            <hr><br/>
            <form method="POST" action="register.php">     
                Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>  
                Nome: <input type="text"  name="name" placeholder="Nome" required/><br/><br/>
                Cognome: <input type="text"  name="surname" placeholder="Cognome" required/><br/><br/>  
                Anno di nascita: 
                    <select name="year">
                        <?php 
                            for($anno=2021; $anno > 1920; $anno--)
                                echo "<option value='$anno'>$anno</option>";
                        ?>
                    </select>    
                    <br/><br/>        
                Password: <input type="password" name="password1" placeholder="Password" required/><br/>
                Conferma Password: <input type="password" name="password2" placeholder="Password" required/><br/>
                
                <input type="submit" name="submit" value="Invia"/>
                <input type="reset" name="cancella" value="Cancella Tutto"/>

                <?php 
                    if(isset($_GET['register']) && $_GET['register']=='success'){
                        echo "<p style='color: green'>REGISTRAZIONE AVVENUTA CON SUCCESSO</p>";
                    }


                    if(isset($_GET['error']) && $_GET['error']=='register'){
                        echo "<p style='color: red'>ERRORE IN REGISTRAZIONE</p>";                        

                        if(isset($_GET['pwd']) && $_GET['pwd']=='different')
                            echo "<p style='color: red'>Le password inserite non corrispondono</p>";
                    }
                ?>
            </form>
        </div>
    </body>
</html>