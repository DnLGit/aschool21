<!DOCTYPE html>
<html>
  <head>
    <title>MyDevis</title>
    <!--Referenza a contenuto da scaricare presente sullo stesso server-->
    <link rel="icon" href="icons/spo.png" />

    <!--CSS locale-->
    <link rel="stylesheet" href="css/my_style.css" />
  </head>
  
  <body>    

      <!--IMPORTANTE!!! id UNIVOCO per ogni tag all'interno della pagina-->
      <div id="intestazione" class="text-red">
        La pagina di 
        <span class="text-bold hg-yellow">
          <?php 
              session_start();

              if(isset($_GET['n']))//esiste un parametro 'n' nella chiamata GET?
                echo($_GET['n']);//se sì stampa quello
              else
                echo("Deviss");//altrimenti Deviss
          ?>
        </span>
      </div>

      <p>
        <?php 
          if(isset($_SESSION['nome']))
            echo("NOTA: ti se già autenticato come " . $_SESSION['nome']);
        ?>
      </p>

      <hr class="mb5"/>

      <h4> GET FORM </h4>
      <form method="GET" action="processForm.php">
        <input type="text" name="nome" placeholder="Inserisci nome" value=""/>
        <br/>
        <input type="number" min="0" step="1" name="eta" placeholder="Inserisci eta" value=""/>
        <br/>
        <input type="submit" value="Invia"/>
      </form>

      <h4> POST FORM </h4>
      <form method="POST" action="processForm.php">
        <input type="text" name="nome" placeholder="Inserisci nome" value=""/>
        <br/>
        <input type="number" min="0" step="1" name="eta" placeholder="Inserisci eta" value=""/>
        <br/>
        <input type="submit" value="Invia"/>
      </form>

      <hr class="mt5"/>

      <!--Referenza a contenuto da scaricare su un server remoto-->
      <img src="https://store-images.s-microsoft.com/image/apps.39852.14455250034361967.824623c3-91b6-402f-9a68-c66bfac9bc20.7bfae929-5b2a-4efe-9639-5ffd1e2de297?mode=scale&q=90&h=720&w=1280&background=%23FFFFFF" />
      <p id="GokuDidasc" class="text-red">Goku</p>

      <!-- come noti dallo span sopra è possibile attribuire più classi CSS allo stesso tag -->
      <!-- ma è anche possibile avere diversi tag a cui è stata associata la stessa classe 
        (text-red a "GokuDidasc" e a "intestazione")-->

      <?php 

        function nl(){
          echo("<br/>");
        }

        for($i=0; $i<10; $i++){
          echo("<p class=\"text-bold\">Hello World". $i ."!</p>");  
          if($i % 2 == 1)
            nl();
        }
      ?>
  </body>

</html>