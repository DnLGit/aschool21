<!DOCTYPE html>
<html>
  <head>
    <title>MyDevis</title>
    <!--Referenza a contenuto da scaricare presente sullo stesso server-->
    <link rel="icon" href="icons/spo.png" />

    <!--CSS locale-->
    <link rel="stylesheet" href="css/my_style.css" />
  </head>
  
  <body>    
        <?php
            session_start();//avvia la sessione

            if(isset($_GET['nome']) && isset($_GET['eta'])){//esistono i parametri GET 'nome' e 'eta'
                    $nome = $_GET['nome'];
                    $eta = intval($_GET['eta']);//parse a intero
                    if($eta < 30)
                        echo "Complimenti ".$nome.", sei ancora giovane!";
                    else
                    echo "Caro/a il mio/a ".$nome.", inizi ad avere una certa età!";

                    $_SESSION["nome"] = $nome;
            
            }else if(isset($_POST['nome']) && isset($_POST['eta'])){//esistono i parametri POST 'nome' e 'eta'
                $nome = $_POST['nome'];
                $eta = intval($_POST['eta']);//parse a intero
                if($eta < 30)
                    echo "Complimenti ".$nome.", sei ancora giovane!";
                else
                echo "Caro/a il mio/a ".$nome.", inizi ad avere una certa età!";

                $_SESSION["nome"] = $nome;
            
            }else{
                echo "<h3 class='text-red'>PARAMETRI MANCANTI</h3>";
            }
            
        ?>
        <br/>
        <a href="prova.php">Torna indietro</a>
  </body>

</html>