<?php
    session_start();
    session_destroy();

    header("location:index.html");//redirect automatico a index.php con metodo header

    //alternativa in HTML (no PHP)
        /*echo ("<meta http-equiv=\"refresh\" content=\"0; URL='index.php'\" />")*/
?>

<!--
alternativa in HTML per redirect automatico 

<meta http-equiv="refresh" content="0; URL='index.php'" />
-->