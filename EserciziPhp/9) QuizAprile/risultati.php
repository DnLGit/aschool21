<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Risultati </title>
        <link rel="icon" href="images\lineaCred.jpg"/>
        <link rel="stylesheet" href="myStyle.css"/>
    </head>

<body>
    <h1> Ecco i Risultati </h1>

    <?php
        session_start();

        if (isset($_SESSION["u_logged"])){
            echo "ciao " . $_SESSION["u_logged"] . "</br> guarda qui: </br></br>";
        }
    ?>

<?php
    $r0 = $_POST["risposta0"];      // msg per Davis: inizialmente avevo utilizzato direttamente i $_POST ...
    $r1 = $_POST["risposta1"];      // ... pero' mi dava errori "Undefined array key "risposta0", ho quindi ...
    $r2 = $_POST["risposta2"];     // ... memorizzato tutto in variabili "$r" pero' idem.
  
    $Risp=0;
    if($r0 == "c")$Risp++;
    if ($r1 == "b")$Risp++;
    if ($r2 == "c")$Risp++;
    $Percento = ($Risp/3)*100;

    $io=$_SESSION["u_logged"];  // msg per Davis: anche qui avevo inizialmente utilizzato $_SESSION["u_logged"] ...
                                // ... e mi dava errore nella echo qui sotto ... pero' con la variabile no, perché?
    echo "<h4><font ='verdana' color ='blue'> 
        Ciao $io </br>
        Hai risposto bene a $Risp domande </br>
        Statisticamente sei al $Percento %"

  
?>
    </br></br>
    <a href quiz.php> Ripeti il quiz</a></br>    <!-- msg per Davis: in entrambi questi link mi da lo stesso errore sopra -->
    <a href index.php> Torna alla Home</a>

</body>
</html>