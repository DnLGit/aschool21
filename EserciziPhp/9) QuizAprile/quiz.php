<!DOCTYPE html/>
<html>
    <head>
        <meta charset = "utf-8">
        <title>La pagina dei Quiz</title>
        <link rel="icon" href="images/lineaCred.jpg"/>
    </head>
<body>

    <?php

    /*  memorizzazione credenziali utenti su array */
        $uDnL = array("user"=>"DnL", "pwd"=>"abc");
        $uAsia = array("user"=>"Asia", "pwd"=>"def");

        $utenti = array($uDnL, $uAsia); // array dei due array sopra

        /* sarebbe potuto essere anche:  
            $utenti = array(array("user"=>"DnL", "pwd"=>"abc"), array("user"=>"Asia", "pwd"=>"def"));                      */

        /*  $logins = array("DnL" => "abc", "Asia" => "def",);
        -->> Ok: "DnL" é la chiave, "abc" il valore della password, ma perché il prof. usa questa sintassi in array ??     */
        

        session_start();                                            // inizio sessione

        if(isset($_POST["username"]) && isset($_POST["password"])){ // controlla/valida l'input ricevuto da pagina del form 
            for($i=0; $i<count($utenti); $i++){                // se ci sono dati si entra nel ciclo per memorizzare lo stato
                $u = $utenti[$i];                          // memorizza il valore iesimo dell'array utenti nella variabile $u
                if($u["user"] == $_POST["username"] && $u["pwd"] == $_POST["password"]){      
                    $_SESSION["u_logged"] = $u["user"];
                }             // confronta i dati in arrivo dal client con quelli memorizzati nel server (username e pssword)
            }                // se credenziali confermate salva il valore dello "user" nella sessione (nell'array di SESSION)
        }

        if(isset($_SESSION["u_logged"])){              // se utente loggato (true) stampa di Benvenuto all'utente loggato ...
            echo "Benvenuto" .$_SESSION["u_logged"]. "!";
    ?>
        <form method="POST" action="risultati.php">                                     <!-- ... e segue la form del quiz -->

            <h3> Quizzone di Geografia </h3>
            <b> 1) Qual'è la capitale della Francia? </b></br>
            <input type="radio" name="risposta0" value="a"/> Roma</br>
            <input type="radio" name="risposta0" value="b"/> Versailles</br>
            <input type="radio" name="risposta0" value="c"/> Parigi</br></br>

            <b> 2) Che fiume attraversa la capitale della Francia? </b></br>
            <input type="radio" name="risposta1" value="a"/> Tamigi</br>
            <input type="radio" name="risposta1" value="b"/> Senna</br>
            <input type="radio" name="risposta1" value="c"/> Don</br></br>

            <b> 3) Con chi confina a sud la Francia? </b></br>
            <input type="radio" name="risposta2" value="a"/> Olanda</br>
            <input type="radio" name="risposta2" value="b"/> Portogallo</br>
            <input type="radio" name="risposta2" value="c"/> Spagna</br></br>

            <br/></br></br>
            
            <input type="submit" name="inviaRisposte" value="INVIA"/>
        </form>
        </br></br>
        <a href="logout.php">Logout</a>
    
    <?php
        }else{                                                               // altrimenti stampa messaggio "Non autorizzato"
            echo "Non autorizzato</br><a href='index.php'>Torna Indietro</a>";    // con link (referenza?) torna indietro ...
        }
    ?>

</body>
</html>
