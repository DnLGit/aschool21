<!DOCTYPE html>
<html>
    <head>
        <title> Form 1 </title>
        <meta charset="UTF-8"/>
        <link rel="icon" href="images/lineaCred.jpg"/>
        <link rel="stylesheet" href="mystyle.css"/>        
    </head>
    <body>
        <div>
            <h1> Visualizza i Corsi</h1>
                <?php
                    if(!isset($_POST['username']) || !isset($_POST['password']) ||
                    !isset($_POST['corso']) || !isset($_POST['come'])){    //manca qualche parametro?
                        
                        echo "Accesso non valido<br/><a href='Pag.1 Form.html'>Torna Indietro</a>";
                        die();//non vado avanti con lo script
                    }
                    
                    $nome=$_POST['username'];       // memorizza dati e scelte dell'array POST sulle quattro variabili
                    $psw=$_POST['password'];
                    $corsi=$_POST['corso'];
                    $come=$_POST['come'];

                    echo "Bene $nome, sei entrato con la password $psw <br/><br/>";
                    echo "Hai scelto di frequentare i corsi di: <br/>";
                    foreach($corsi as $cs){
                        echo $cs . "<br/>";
                    }

                    echo "in modalità: $come";
                ?>  
        </div>
    </body>



</html>