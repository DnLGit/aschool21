<?php
	session_start();
	
	if(isset($_POST['Submit']))
	{
		//definiamo nome utente e password da memorizzare nell'array
		$logins = array('Giancarlo' => '123456', 'username1' => 'password1', 'username2' => 'password2');
		
		$Username = isset($_POST['Username']) ? $_POST['Username'] : '';
		$Password = isset($_POST['Password']) ? $_POST['Password'] : '';
		
		if(isset($logins[$Username]) && $logins[$Username] == $Password)
		{
			//operazione con successo: setta le variabili session e fai un redirect alla pagina riservata
			$_SESSION['UserData']['Username']=$logins[$Username];
			header("location:index.php");
			exit;
		} 
		else
		{
			//operazione senza successo: messaggio di errore
			$msg="<span style='color:red'>Login non valido!</span>";
		}
	}

?>



<html>
<head>
	<meta charset="utf-8">
	<title> Login con credenziali salvate su array</title>
	<link href="./css/style.css" rel="stylesheet">
</head>

<body>
	<div id="fr1">
		<h1>Pagina di login</h1>	
	</div>
	<br>
	<form action="" method="post" name="login_form">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="1" class="Table">
		<?php if(isset($msg)) { ?>
		
		<tr>
			<td colspan="2" align="center" valign="top"><?php echo $msg; ?></td>
		</tr>
		<?php } ?>
		
		<tr>
			<td colspan="2" align="left" valign="top"><h3>Login</h3></td>
		</tr>
		<tr>
			<td align="right" valign="top">Username</td>
			<td><input name="Username" type="text" class="Input"></td>
		</tr>
		<tr>
			<td align="right">Password</td>
			<td><input name="Password" type="password" class="Input"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input name="Submit" type="submit" value="Login" class="Button3"></td>
		</tr>
		</table>
	</form>
	

	


</body>
</html>