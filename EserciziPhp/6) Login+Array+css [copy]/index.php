<?php
	session_start();
	
	if(!isset($_SESSION['UserData']['Username']))
	{
		header("location:login.php");
		exit;
	}

?>

Congratulazioni! Benvenuto nella tua pagina riservata. <a href="logout.php"> Clicca qui </a> per il logout.