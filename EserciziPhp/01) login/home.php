<?php

echo "<h1> Home del sito </h1>";

/* qui viene aperta una sessione per fare in modo che si possa accedere alla home dell'account inserendo le credenziali, altrimenti ritorna un errore, perché si tenta di accedere senza fare il login */

$flag = false;                                              /* variabile "flag" settata a "false" */
session_start();                                            /*  qui parte la sessione */    

if(isset($_POST["utente"]) && isset ($_POST["password"]))   /* se le variabili del form vengono dichiarate ... */
{
    $utente = $_POST["utente"];                             /* memorizzo i valori in queste variabili */
    $password = $_POST["password"];
    $_SESSION["utente"] = $utente;                          /* e nelle SESSION per ricordarle poi ... */
    $_SESSION["password"] = $password;
}

elseif(isset($_SESSION["utente"]) && isset($_SESSION ["password"])) /* se le $_SESSION vengono dichiarate ... */
{
    $utente = $_SESSION ["utente"];                                 /* RI-memorizzo $_SESSION in questA variabilE */
    echo "Benvenuto utente $utente";                                /* e do il benvenuto all'utente */
}

else                                                                /* altrimenti genera l'errore ...  */
{
    echo "Errore, stai tentando di accedere direttamente senza login!!";
    $flag = true;
}

if(!$flag)
{
    echo "<p> &nbsp </p>";
    echo "<a href = 'pag1.php'> Vai a pagina 1 </a>";
    echo "<br><br><a href = 'pag2.php'> Vai a pagina 2</a>";
    echo "<p> &nbsp</p>";
    echo "<a href = 'login.html'> Logout</a>"; 
}

?>