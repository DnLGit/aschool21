<!doctype html>
<html>
<head><title> VariabiliOperatori </title><head>
<body>
<h1> Variabili e Operatori in PHP </h1>


<?php
// Le variabili sono identificate da un nome preceduto dal "$"
// Php attribuisce in modo dinamico il Tipo alla variabile

$eta = 19;										// assegnazione di un intero ad una variabile
$titolo = "I Promessi Sposi";					// assegnazione di una stringa
$prezzo = 31.2;									// assegnazione di un float
echo "Lui ha " . $eta . " anni" . "<br/> <p>"; 
echo "da giovane ha scritto: " . $titolo . " da solo, aveva solo " . $eta . " anni <br/>";
echo "all'epoca ha ricevuto " . $prezzo . " " . "misere Lire" . "<br/><br/>";
// Posso scegliere di fare gli spazi come voglio!!!
?>

<h2> Info apici singoli e doppi </h2>
<?php
$etadoppia = $eta*2;
$prezzodoppio = $prezzo*2;


echo "frase con apici \" doppi: <br/>";
echo "Ho scritto $titolo fino a $etadoppia anni <br/>";
echo "Ora che ne ho  $etadoppia, guadagno $prezzodoppio all'ora <br/><br/>";

echo "frase con apici singoli \': <br/>";
echo 'Ora che ne ho  $etadoppia, guadagno $prezzodoppio all\'ora <br/><br/>';


?>


</body>
</html>