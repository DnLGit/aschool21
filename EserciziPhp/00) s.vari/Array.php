<!doctype html>
<html>
<head>
<title> Array </title>
</head>
<body>
<h2> Array: Array numerici </h2>
<hr />

<?php

$voti = array(8.0, 6.5, 5.5, 7.0);
$dati = array(30, "Roma", 12.7, true, null);


// echo "Funzione -> var_dump($voti) = "; <-- questa mi da un "Warning" ... perché??
echo "Funzione => var_dump dell'array voti:  ";
echo var_dump($voti) . "<br/>";

echo "Funzione => var_dump dell'indice zero array voti: ";
// echo "Funzione -> var_dump($voti[0]) = "; <-- stesso "Warning" ... 
echo var_dump($voti[0]) . "<br/><br/>";


echo "Funzione => var_dump dell'array dati:  ";
echo var_dump($dati) . "<br/>";

echo "Funzione => var_dump dell'indice uno array dati: ";
echo var_dump($dati[1]) . "<br/></br>";

echo "Ora Funzione => var_dump degli altri indici array dati: </br>";
echo "Indice due: " . var_dump($dati[2]) . "<br/>";
echo "Indice tre: " . var_dump($dati[3]) . "<br/>";

echo "Indice quattro: " . var_dump($dati[4]);	// perché stampa con ordine inverso??
echo "<br/><br/>";
echo "Indice quattro: ";
echo var_dump($dati[4]);	// perché non posso concatenare "in ordine"??
echo "<br/><br/>";
?>


<h2> Array: Cicli con Array Numerici e Associativi </h2>
<hr />

<h3> Array: Array numerico: </h3>
<?php

$voti = array(8.0, 6.5, 5.5, 7.0);
$dati = array(30, "Roma", 12.7, true, null);

echo "stampo indice zero => " . $dati[0];
echo "<br/><br/>";

echo "stampo tutto array associativo dati con ciclo While: <br/>";
$n = 0;
while ($n<4){
	echo "index " . $n . ": " . $dati[$n] . "<br/>";
	$n++;
}

echo "<br/>";

echo "Oppure, lo con ciclo foreach: <br/>";

foreach ($dati as $nome){
	echo $nome . "<br/>";	
}

?>

<h3> Array: Array associativo: </h3>
<?php


$dati = array(30, "Roma", 12.7, true, null);

$dati["numero civico"] = 30;
$dati["Citta'"] = "Roma";
$dati["Punteggio"] = 12.7;
$dati["check"] = true;
$dati["extra"] = null;

echo "Marco abita a " . $dati["Citta'"] . "al n° " . $dati["numero civico"];




?>










