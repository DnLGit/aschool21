-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 20, 2021 alle 00:05
-- Versione del server: 10.4.18-MariaDB
-- Versione PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testapp`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `bobine`
--

CREATE TABLE `bobine` (
  `IdCoil` int(11) NOT NULL,
  `tipo` varchar(8) NOT NULL,
  `nomeC` varchar(10) DEFAULT NULL,
  `zona` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bobine`
--

INSERT INTO `bobine` (`IdCoil`, `tipo`, `nomeC`, `zona`) VALUES
(1, 'asdf', 'afdsfsd', 'ciliegie'),
(2, 'dewfew', 'fewfew', 'gfrefer');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `bobine`
--
ALTER TABLE `bobine`
  ADD PRIMARY KEY (`IdCoil`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `bobine`
--
ALTER TABLE `bobine`
  MODIFY `IdCoil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
