-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 21, 2021 alle 09:01
-- Versione del server: 10.4.18-MariaDB
-- Versione PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_es_devis`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `pagamenti`
--

CREATE TABLE `pagamenti` (
  `Cc` varchar(15) NOT NULL,
  `Iban` varchar(30) DEFAULT NULL,
  `CrCard` varchar(16) DEFAULT NULL,
  `Banca` varchar(30) NOT NULL,
  `Pagato` tinyint(1) DEFAULT NULL,
  `codeFk` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `username` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `nome` varchar(32) NOT NULL,
  `cognome` varchar(32) NOT NULL,
  `anno_nascita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`username`, `password`, `nome`, `cognome`, `anno_nascita`) VALUES
('ddd12', 'f2f6bbc18d64a0342fdad343dfe3973f57adc8c16f917c5735c2c0c858791358', 'Devis', 'Dal ', 2014),
('devis', '6c51ed231f6e96f76fc216b9bb093988806819fffe0a5be3fbc4764e3ccee8d1', 'Devis', 'Dal Moro', 1997),
('DnL', '5bd34386f06c1f0576bd686fdba73f88026f02cf023b936dca70b98e858793e0', 'Daniele', 'Bergamasco', 1902),
('user', 'd7cb62855cc3a04933d835db565be339b4727bab711fb4d7bc277538709b1d32', 'nome', 'cognome', 2000);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti2`
--

CREATE TABLE `utenti2` (
  `Code` varchar(10) NOT NULL,
  `Cf` varchar(16) NOT NULL,
  `Nome` varchar(10) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Via` varchar(30) DEFAULT NULL,
  `Cap` varchar(8) DEFAULT NULL,
  `Citta` varchar(30) DEFAULT NULL,
  `Nazione` varchar(2) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `User` varchar(16) NOT NULL,
  `Psw` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `pagamenti`
--
ALTER TABLE `pagamenti`
  ADD PRIMARY KEY (`Cc`),
  ADD KEY `codeFk` (`codeFk`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `utenti2`
--
ALTER TABLE `utenti2`
  ADD PRIMARY KEY (`Code`);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `pagamenti`
--
ALTER TABLE `pagamenti`
  ADD CONSTRAINT `codeFk` FOREIGN KEY (`codeFk`) REFERENCES `utenti2` (`Code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
