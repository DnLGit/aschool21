<?php 

if(isset($_POST['nome']) && isset($_POST['password'])){

	$logins = array(
	'pippo' => 'pippo',
	'pluto' => 'pluto'
	);
	$Username = isset($_POST['nome']) ? $_POST['nome'] : '';
	$Password = isset($_POST['password']) ? $_POST['password'] : '';


	if (isset($logins[$Username]) && $logins[$Username] == $Password){
		header("location:connesso.php");
		exit;
	}
}
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Esercizio Login Array</title>
	</head>
	<body>
		<form action="login.php" method="POST" name="scelta">
			<table border="1">
				<tr>
					<td colspan="2" align="left" valign="top"><h3>Seleziona scelta</h3></td>
				</tr>
				<tr>
					<td align="right" valign="top">Nome:</td>
					<td><input type="text" name="nome" required></td>
				</tr>
				<tr>
					<td align="right">Password:</td>
					<td><input type="password" name="password" required></td>
				</tr>
				<tr>
					<td><input type="submit" name="invia" value="Invia!"></td>
					<td><input type="reset" name="cancella" value="Cancella tutto!"></td>
				</tr>
			</table>
		</form>
	</body>
</html>