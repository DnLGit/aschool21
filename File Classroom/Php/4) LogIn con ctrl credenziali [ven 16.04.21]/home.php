<?php

echo "<h1>Home del sito</h1>";

$flag = false;
session_start();

if(isset($_POST["utente"]) && isset($_POST["password"]))
{
	$utente = $_POST["utente"];
	$password = $_POST["password"];
	$_SESSION["utente"] = $utente;
	$_SESSION["password"] = $password;
}

elseif(isset($_SESSION["utente"]) && isset($_SESSION["password"]))
{
	$utente = $_SESSION["utente"];
	echo "Benvenuto utente $utente";
}

else
{
	echo "Errore, stai tentando di accedere direttamente senza login!";
	$flag = true;
}

if(!$flag)
{
	echo "<p>&nbsp</p>";
	echo "<a href='pagina1.php'>Vai a pagina 1</a>";
	echo "<br><br><a href='pagina2.php'>Vai a pagina 2</a>";
	echo "<p>&nbsp</p>";
	echo "<a href='login.html'>Logout</a>";
}
?>