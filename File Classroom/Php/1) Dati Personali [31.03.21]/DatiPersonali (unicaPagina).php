/* Codice che integra in un'unica pagina il form HTML per l'inserimento dei dati da parte dell'utente  
   e script PHP che li visualizza. In caso di dati mancanti termina l'esecuzione visualizzando un messaggio di errore.
   Si fa uso anche della funzione htmlentities allo scopo di effettuare la trasformazione di una stringa di testo in una
   stringa coerente con le convenzioni HTML  (testo aggiunto da Classroom) */


<html>
 <head>
  <title>Dati personali</title>
 </head>
 <body>
  <?php
    if (!isset($_POST["invio"])) { // verifica invio dati
  ?>
  <form method="POST" action="DatiPersonali.php">
  Cognome: <input type="text" size="12" maxlength="24" name="cognome"><br>
  Nome: &nbsp&nbsp&nbsp&nbsp <input type="text" size="12" maxlength="24" name="nome"><br><br>
  Sesso: <br>
  Maschio: &nbsp<input type="radio" value="maschio"name="sesso"><br>
  Femmina: <input type="radio" value="femmina" name="sesso"><br><br>
  Scegli i cibi che preferisci:<br>
  Bistecca: <input type="checkbox" value="bistecca" name="cibo[]"><br>
  Pizza: &nbsp&nbsp&nbsp&nbsp <input type="checkbox" value="pizza" name="cibo[]"><br>
  Pollo: &nbsp&nbsp&nbsp&nbsp <input type="checkbox" value="pollo" name="cibo[]"><br><br>
  <textarea rows="5" cols="20" name="citazione">
Inserisci la tua citazione preferita!
  </textarea><br><br>
  Seleziona il tuo livello di istruzione scolastica:<br>
  <select name="istruzione">
    <option value="scuola media">Medie</option>
    <option value="scuola superiore">Superiori</option>
    <option value="universitÓ">Studi universitari</option>
  </select><br><br>
  Seleziona la parte del giorno che preferisci:<br>
  <select name="partegiorno" size="3">
    <option value="mattino">Mattino</option>
    <option value="pomeriggio">Pomeriggio</option>
    <option value="notte">Notte</option>
  </select><br><br>
  <input type="reset" value="Reset">
  <input type="submit" value="Ok" name="invio">
  </form>
  <?php
 }
 else {
 if ( !isset($_POST["cognome"]) || !isset($_POST["nome"]) || !isset($_POST["sesso"]) || !isset($_POST["cibo"]) ||  !isset($_POST["citazione"]) || !isset($_POST["istruzione"]) || !isset($_POST["partegiorno"]))
   die("Dati personali non corretti.");
  $nome = htmlentities( $_POST["cognome"], ENT_HTML5,'ISO-8859-1');
  $cognome = htmlentities( $_POST["nome"], ENT_HTML5,'ISO-8859-1');
  $sesso = htmlentities( $_POST["sesso"], ENT_HTML5,'ISO-8859-1');
  $cibo = $_POST["cibo"]; // array
  $citazione = htmlentities( $_POST["citazione"], ENT_HTML5,'ISO-8859-1');
  $istruzione = htmlentities( $_POST["istruzione"], ENT_HTML5, 'ISO-8859-1');
  $partegiorno = htmlentities( $_POST["partegiorno"], ENT_HTML5, 'ISO-8859-1');
  if ( strlen($nome) == 0 || strlen($cognome) == 0 || count($cibo) == 0 || strlen($citazione) == 0)
    die("Dati personali non completi.");
  echo "Ciao, $cognome $nome<br>";
  echo "Sei $sesso e ti piace mangiare:<br><br>";
  foreach ($cibo as $c) {
    echo htmlentities($c, ENT_HTML5, 'ISO-8859-1')."<br>";
  }
  echo "<br>La tua citazione preferita &egrave:<br>";
  echo "<em>$citazione</em><br><br>";
  echo " Ti senti pi&ugrave a tuo agio di: $partegiorno ed il tuo livello di istruzione &egrave: $istruzione<br>";
 }
 ?>
 </body>
</html>

