<?php
	require "session.php";
?>
<html>
  <head>
    <title>DB & PHP test</title>
  </head>
  <body>
  <?php
	// connessione al database con credenziali di sessione
	$conn = mysqli_connect("localhost",$_SESSION['DB_username'],$_SESSION['DB_password'],"Disneyland");
	$query = " SELECT nome,denominazione FROM Personaggi,Citta 
				WHERE Personaggi.citta = Citta.sigla ORDER BY nome";
	$result = mysqli_query($conn, $query);
	if (mysqli_num_rows($result) != 0) {
		echo "<table border>";
		echo "<tr>";
		echo "<th>Personaggio</th>";
		echo "<th>Citt&agrave;</th>";
		echo "</tr>";
		while ($row = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>$row[nome]</td>";
			echo "<td>$row[denominazione]</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
	//$result->free();
	mysqli_close($conn);
  ?><br>
    <a href="http://localhost/Disneyland/add.php">
      Aggiungi un nuovo personaggio.
    </a><br>
    <a href="http://localhost/Disneyland/del.php">
      Elimina un personaggio esistente.
    </a><br>
   
  <?php
	echo " <a href='http://localhost/Disneyland/logout.php'> [$_SESSION[username] logout]</a>";
  ?>
 </body>
</html>
