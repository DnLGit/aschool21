<html>
	<head>
		<title>Dati</title>
	</head>
	<body>
		<form method="GET" action="filters.php">
			<label for="nominativo">Nominativo</label>
			<input id="nominativo" name="nominativo" type="text" required><br>
			<label for="email">Indirizzo posta elettronica</label>
			<input id="email" name="email" type="text" required><br>
			<label for="web">Sito web</label>
			<input id="web" name="web" type="text" required><br>
			<label for="altezza">Altezza (cm)</label>
			<input id="altezza" name="altezza" type="text" required><br>
			<label for="peso">Peso (Kg)</label>
			<input id="peso" name="peso" type="text" required><br>
			<input type="submit" value="Inoltra i dati">
		</form>
		
	<?php
		$nominativo = filter_input(INPUT_GET, 'nominativo', FILTER_SANITIZE_STRING);
		if($nominativo)
			echo "Nominativo: $nominativo";
		else
			echo "Nominativo non corretto!";
		echo "<br>";
		
		$email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL);
		if($email)
			echo "Indirizzo di posta elettronica: $email";
		else
			echo "Indirizzo di posta elettronica non corretto!";
		echo "<br>";
		
		$web = filter_input(INPUT_GET, 'web', FILTER_VALIDATE_URL);
		if($web)
			echo "Sito web: $web";
		else
			echo "Sito web non corretto!";
		echo "<br>";
		
		$altezza = filter_input(INPUT_GET, 'altezza', FILTER_VALIDATE_INT);
		if($altezza)
			echo "Altezza: $altezza cm";
		else
			echo "Altezza non corretta!";
		echo "<br>";
		
		$peso = filter_input(INPUT_GET, 'peso', FILTER_VALIDATE_FLOAT);
		if($peso)
			echo "Peso: $peso Kg";
		else
			echo "Peso non corretto!";
		echo "<br>";
		
	?>
</body>
</html>