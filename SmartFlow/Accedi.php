<!DOCTYPE html>
<html>    
    <head><title> accedi-SmartFlow </title></head>
</html>

<?php
    //  carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php";


    /*  https://tryphp.w3schools.com/showphpfile.php?filename=demo_db_select_proc
        QUI esempio PER AVERE CODICE GENERALE DI UNA SELECT DA PHP   */



    if(!isset($_POST["username"]) || !isset($_POST["password"]))
    {
        header("location:signin.php?error=login");       // invio tramite GET su URL della pagina index.php di eventuale messaggio di errore
        die();                                           // si potrebbe fare anche con un "echo", SENZA REDIRECT AUTOMATICO!!                                                          
    }                                                    // echo "Login errore! <a href=\"signin.php\">immetti i dati</a>";

    $user = $_POST["username"];
    $pwd = $_POST["password"];

    $pwd = hash('sha256', $pwd);

    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //connessione server al dbms (ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
    $query = "SELECT code, user, psw, nome, cognome FROM utenti WHERE user = '$user' AND psw = '$pwd'";
    
    // la risposta alla chiamata mysqli_query, in caso venga fatta una select, contiene le righe selezionate 

    $result = mysqli_query($conn, $query);  //c onn + query -> server richiede al dbms di eseguire $query

    //  mysqli_num_rows(result) dove result è il risultato della SELECT e ritorna il numero di righe recuperate
    if (mysqli_num_rows($result) == 1) 
    {
        //  login avvenuto con successo
        //  login andato a buon fine -> creo una sessione
        session_start();

        //  metti dentro a $row la riga ricevuta in risposta
        $row = mysqli_fetch_assoc($result); 
        
        //  salvo nelle variabili di sessione i dati dell'utente contenuti nella riga appena ricevuta dal db in risposta alla SELECT
        $_SESSION['username'] = $row['user'];
        $_SESSION['nome'] = $row['nome'];
        $_SESSION['cognome'] = $row['cognome'];
        $_SESSION['code'] = $row['code'];

        header("location:myaccount.php");

    } 
    else 
    {   // login errato perché la SELECT non ha trovato un matching, una riga con quell'username e quella hash.
        // echo "Login errore! <a href=\"index.php\">Riprova</a>"; // anche qui si potrebbe fare così (SENZA REDIRECT AUTOMATICO!!)
        header("location:signin.php?error=login");
    }

    mysqli_close($conn);    /*  CHIUDO la connessione tra db e server (per questioni di occupazione memoria)    */ 
?>