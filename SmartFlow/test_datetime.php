<?php
    //carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php"; 
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato


    if(!isset($_SESSION['username']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }  

    /*  se username presente nelle variabili di sessione, è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<!DOCTYPE html>
<html>
    <head><title> dt-SmartFlow </title>
    </head>
    <body style="text-align: center;">

        <?php                   //   ripeto l'operazione sopra ... per scegliere "quanto ricaricare" a seconda del tempo.
            //$batt = NULL;
            $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);
            $user = $_SESSION['username'];
            $query = "SELECT batteria  FROM veicoli, utenti
            WHERE veicoli.proprietario_fk=utenti.code AND utenti.user='$user'";                              /*    */
            $result = mysqli_query($conn, $query);
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                $batt=$row['batteria'];                             
            }
            echo $batt;
        ?>


        <?php 
            if(!isset($_POST['start']) || !isset($_POST['end']))
            {
        ?>
                <form method="POST" action="test_datetime.php">
                    <label for="startDT">Inizio:</label>
                    <input id="startDT" type="datetime-local" name="start" min=""/><br/><br/>

                    <label for="endDT">Fine:</label>
                    <input id="endDT" type="datetime-local" name="end" min=""/><br/><br/>

                    <input type="submit" value="Calcola"/>
                </form>
        
        <?php
            }else{

                //var_dump($_POST['start']);
                //var_dump($_POST['end']);
                $start = strtotime($_POST['start']);    //  strtotime(): funzione che converte in formato "timestamp" un valore di tempo che gli viene passato.
                $end = strtotime($_POST['end']);        //  timestamp: rappresenta il numero di secondi trascorsi dal 01.01.1970 (da usare per il confronto). 
                
                // Formulate the Difference between two dates
                $diff = abs($start - $end);
                $watt= ($batt*$diff)/7200;
                $costo=$watt*0.45;

        ?>

                CALCOLO: <?= $diff, $watt, $costo?> w 
        <?php
            }
        ?>

    </body>
</html>