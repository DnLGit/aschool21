<?php
    //carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php"; 
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato


    if(!isset($_SESSION['username']) || !isset($_SESSION['code']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }  
    $code_utente = intval($_SESSION['code']);

    /*  se username presente nelle variabili di sessione, è per forza la sua e quindi significa che si è autenticato correttamente */
?>
<!DOCTYPE html>
<html>
    <head>
        <title> MyAccount-SmartFlow </title>
        <?php 
                // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
                require "head.php"; 
        ?>
    </head>
    <body class="text-center mx-auto">
        <hr>
        <div class="row">                       <!-- class="row" (bootstrap)  e vedi anche:  "text-center mx-auto"   -->
            <div class="col-2"><a href='index.php'><img src="images/SmartFlow Logo (rif01).jpg" class="img-fluid" alt="LogoSfW"></a></div>
            <div class="col-6"></div>
            <div class="col-2"><h5><a href='profilo.php'>Profilo</a></h5></div>
            <div class="col-2"><h5><a href='signin.php'>Sign Out</a></h5></div>
        </div>
        <hr/>

        <h2>MyAccount</h2>                                                              <!--   style="text-align: center;"  -->
        <div>
            Ciao <?=$_SESSION['username']?>
            
            <!-- <?=$_SESSION['nome']." ".$_SESSION['cognome']?> al momento nome e cognome commentati ... forse anche da togliere??? -->
        </div>    
        <br/><br/><br/><br/>
            
        <div class="row">   <!--    bg-dark text-light -->
        <div class="col-1"></div>
            <div class="col-3 text-start">
            <h4> Simulazione </h4><br/>

                <form method= "POST" action="simulazione.php">                      
            
                    <?php                   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
                        $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);
                        $user = $_SESSION['username'];
                        $query = "SELECT targa, proprietario_fk  FROM veicoli";     // trovare soluzione per far caricare a tutti i veicoli ... => ok, fatto!!!
                        $result = mysqli_query($conn, $query);
                        if (mysqli_num_rows($result) > 0) {
                                
                            echo "Veicolo:   
                            <select name='targa'>";                                                         // bg-info fw-bold - "Veicolo (<span class=\"bg-info\">personali evidenziati</span>):
                          

                            $targhe_macchine_mie = array();//elenco delle targhe delle macchine di user
                            $targhe_macchine_terzi = array();//elenco delle targhe delle macchine altrui

                            while ($row = mysqli_fetch_array($result)){
                                if(intval($row['proprietario_fk']) == $code_utente)//sono il proprietario 
                                   array_push($targhe_macchine_mie, $row['targa']);
                                
                                else //NON sono il proprietario di questo veicolo
                                    array_push($targhe_macchine_terzi, $row['targa']);
                            } 
                            
                            
                            //prima inserisco fra le opzioni le mie macchine
                            foreach($targhe_macchine_mie as $targa)
                                echo "<option value=\"$targa\" class=\"fw-bold bg-info\">$targa</option>";
                            
                            //inserisco uno spazio vuoto (disabled per impedire che venga selezionata come scelta della targa)
                            echo "<option value=\"\" disabled>------</option>";

                            //dopo inserisco fra le opzioni le macchine altrui
                            foreach($targhe_macchine_terzi as $targa)
                                echo "<option value=\"$targa\">$targa</option>";
                        }  
                        echo "</select><br>";
                        echo "(<span class=\"bg-info\">I miei sono evidenziati</span>)</br></br>"; 
                                        
                    ?>
                    <?php                   //   ripeto l'operazione sopra ... per selezionare se ricarica Statica o Dinamica 
                        $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);
                        $user = $_SESSION['username'];
                        $query = "SELECT id_bobina, tecnologia, zona  FROM bobine";                               /*   aggiunto "DISTINCT" per eliminare i doppioni */
                        $result = mysqli_query($conn, $query);
                        if (mysqli_num_rows($result) > 0) {
                                
                        echo "Bobina:
                        <select name='bobina'>";
                    
                        while ($row = mysqli_fetch_array($result))
                            echo "<option value=\"$row[id_bobina]\">$row[tecnologia] - $row[zona]</option>";
                        }
                        echo "</select><br><br>";                            
                    ?>
                   

                        La ricarica termina tra:
                            <select name="durata">
                                <option value="15">15 min</option>
                                <option value="30">30 min</option>
                                <option value="45">45 min</option>
                                <option value="60">1 ora</option>
                                <option value="75">1 ora 15 min</option>
                                <option value="90">1 ora 30 min</option>
                                <option value="105">1 ora 45 min</option>
                                <option value="120">2 ore</option>
                            </select>   
                        <br/><br/>
                        <!-- Pagamento<br/> -->
                        Pagamento Immediato: <input type="radio"  name="pagato"  value=1 /><br/>
                        Pagamento BB FineMese: <input type="radio"  name="pagato"  value=0 /><br/><br/>           
          
                    <input type="submit" value="genera Ricarica">
                </form>
                
            </div>
            <div class="col-2"></div>

            <div class="col-5">
            <h4>Le mie Ricariche</h4><br/>               
                <?php                   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
                    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   
                    $user = $_SESSION['username'];
                    $query = " SELECT targa_fk, data_in, data_out, carica, costo, pagato  FROM utenti, ricariche 
                    WHERE utenti.code = ricariche.guidatore_fk AND utenti.user = '$user'";
                    
                    $result = mysqli_query($conn, $query);
                    if (mysqli_num_rows($result) > 0) {
                        echo "<table class='table w-20 mx-auto'>";
                        echo "<thead>
                                <tr>
                                    <th>Targa</th>
                                    <th>Arrivo</th>
                                    <th>Uscita</th>
                                    <th>Ricarica kW</th>
                                    <th>Costo tot</th>
                                    <th>pagato</th>
                                </tr>
                            </thead>";
                        
                        echo "<tbody>"; 
                        while ($row = mysqli_fetch_array($result)) {
                            $dataIn = date_create($row['data_in']); //converto datetime stringa in versione datetime php
                            $dataOut = date_create($row['data_out']); //converto datetime stringa in versione datetime php
                            echo "<tr>";
                                echo "<td>".$row['targa_fk']."</td>";


                                /*  date_format vuole come primo argomento un istanza datetime php e secondo parametro le regole di formattazione della data 
                                (https://www.w3schools.com/php/func_date_date_format.asp)   */

                                echo "<td>".date_format($dataIn,"d/m/Y H:i:s")."</td>"; 
                                echo "<td>".date_format($dataOut,"d/m/Y H:i:s")."</td>";
                                echo "<td>".$row['carica']."</td>";
                                echo "<td>".$row['costo']."</td>";
                                
                                //op ternario (se pagato vale true, scrivo "Sì", altrimenti scrivo "No")
                                echo "<td>". (($row['pagato'])? "Sì": "No") ."</td>"; 
                            echo "</tr>";
                        }
                        echo "</tbody>"; 
                        echo "</table>";
                    }else
                    {
                        echo "  <div>
                                    <h4>Devi ancora effettuare una ricarica con i nostri sistemi!</h4>
                                </div>";
                    }                                            
                    /*
                        Nota:
                            scrivere <?php echo $_SESSION['n']; ?>
                            oppure   <?=$_SESSION['n']?>
                        è la stessa cosa!!
                    */
                ?>
            </div> 
        <div class="col-1"></div>
        <?php 
            if(isset($_GET['update'])){
                if($_GET['update']=='successS'){
                    echo "<div class='alert alert-success'>RICARICA EFFETTUATA</div>";
                }

                if($_GET['update']=='errorS'){
                    echo "<div class='alert alert-danger'>C'E' QUALCHE ERRORE: RIPROVA</div>";
                }
            }
        ?>            
            
        <br/><br/>       
    </body>
</html>