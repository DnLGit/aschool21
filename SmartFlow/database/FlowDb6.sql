CREATE DATABASE smartflow;
USE SmartFlow;

DROP TABLE IF EXISTS contocorrenti;
DROP TABLE IF EXISTS  ricariche;
DROP TABLE IF EXISTS  bobine;
DROP TABLE IF EXISTS  veicoli;
DROP TABLE IF EXISTS utenti;

CREATE TABLE utenti (
    code INT NOT NULL AUTO_INCREMENT,                           -- meglio questo come pk??
    cf VARCHAR(16),
    nome VARCHAR(10) NOT NULL,
    cognome VARCHAR(20) NOT NULL,
    via VARCHAR(30),
    cap VARCHAR(8),                                             -- 8 = max Brasile
    citta VARCHAR(30),
    nazione VARCHAR(2),
    email VARCHAR(50),                                          -- leggo che si potrebbe usare "NVARCHAR" ...!!
    telefono VARCHAR(15), 
    user VARCHAR(16) NOT NULL UNIQUE,                           -- il prof. aggiunge user e psw di accesso al database.
    psw VARCHAR(256) NOT NULL,                                  -- dovrebbe essere criptata, non in chiaro (forse con un hash??!)
    CONSTRAINT codePk PRIMARY KEY (code)
);

INSERT INTO utenti (code, cf, nome, cognome, via, cap, citta, nazione, email, telefono, user, psw) VALUES
    (1002, 'CLMDNY80A01G538M', 'Deny', 'Calamaro', 'R.Carrieri, 98', '84065', 'Piaggine', 'IT', 'deny.calamaro@virgilio.it', '035/441867', 'Deny01', '3026a0ca485e5831657ba0120fa8dd66b3425427bfb0a2be0db743e2305cc7c5'), -- deny
    (1003, 'SRTNBR78L31D719R', 'Norberto', 'Sartelli', 'A.Buttie, 25', '83040', 'Cairano', 'IT', 'norberto.sartelli@katamail.it', '049/920185', 'Norberto02', '!6d973ddc2038bb03bee5c4ab514ec619d5f32131abd7b8777b641facb50c7574'),
    (1004, 'LNNRNI50M18F226E', 'Rino', 'Lanni', 'R.Fulton, 137', '25071', 'Agnosine', 'IT', 'rino.lanni@tiscali.it', '0461/735595', 'Rino03',  '98a286d52359d07bac43fd3513c0f5264eb66e5ed04de368603b2796227f1d01'),
    (1005, 'MRCNTA51S45F698C', 'Anita', 'Marchesano', 'A.Mapelli, 96', '83040', 'Candida', 'IT', 'anita.marchesano@aruba.it', '010/135138', 'Anita04',  '000e02e40e441be798211aaf86d037bacd3dedbc8a6318ca5f9a5c72ad52ffbe'),
    (1006, 'RSGNLN71A55L959K', 'Natalina', 'Resegotti', 'P.Mascagni, 29', '93010', 'Montedoro', 'IT', 'natalina.resegotti@yahoo.com', '02/603198', 'Natalina05', 'b949e991b33e26ccc75b00738e505b5f119301cd2a01d66f15b3e48f93a6bba3'),
    (1007, 'DLMDVS96L11A703T', 'Devis', 'Dal Moro', 'Casale Nuovo, 86', '31030', 'Borso', 'IT', 'devis@mial.com', '340/', 'dev12', '3af3b8d1856188e78ebc9cac0b13753de8e3e093c41cd68733ade7daaccd6c85'),
    (1008, 'BRSNDR72D01L700R', 'Andrea', 'Brisotto', 'Piave, 22', '31020', 'Tezze', 'IT', 'brisotto@gmail.com', '123/456789', 'and123456', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),  -- psw andrea: 123456
    (1009, 'BLBMRC92E15M089R', 'Marco', 'Balbinot', 'Canova, 35', '31019', 'Vittorio Veneto', 'IT', 'marcoB@gmail.com', '321/987654', 'marco', '7c8ccc86c11654af029457d90fdd9d013ce6fb011ee8fdb1374832268cc8d967'),  -- psw Marco: marco     
    (1010, 'MRCFRC79P44M089J', 'Federica', 'Marcuzzi', 'Alemagna, 53', '31019', 'Vittorio Veneto', 'IT', 'federicaM@gmail.com', '312/896745', 'federica', 'ca9327c81f7c76ed12e5408d12c1c9a1a38ded25d4d73da45d789544a89a965d'),  -- psw Federica: federica
    (1011, 'DNRPLA80M15M089K', 'Paolo', 'De Nardi', 'Zara, 11', '31019', 'Vittorio Veneto', 'IT', 'paoloDN@gmail.com', '132/869754', 'paolo', '254835d73cc88095a30fc74133beabe9d8463b2954493227b205ea326c8a9c86');  -- psw Paolo: paolo
    -- da 01.06.21 in poi
    (1012, 'FRSMTT93M15M089D', 'Matteo', 'Frassinelli', 'Via della Seta, 23/15', '31019', 'Vittorio Veneto', 'IT', 'matteoF@gmail.com', '0438/8967947', 'matteo', 'fe301eaaac49b4652b8dfd9fb0e913683ac5600f59370a6261824ab608b4fad7');  -- psw Matteo: matteo
    (1013, 'CLTMLE81R15G813V', 'Emilio', 'Celotto', 'Via Messigno, 42', '80045', 'Pompei', 'IT', 'emilioC@gmail.com', '081/8638865', 'emilio', '38049c3213a81a528c2a27d198f8e012841fb7d5cbb622227f999e30729286ce');  -- psw Emilio: emilio
    (1014, 'DRDPLA96L15M089H', 'Paolo', 'Da Rodda', 'Via Menarè, 150', '31015', 'Scomigo', 'IT', 'paoloDR@gmail.com', '0438/3884459', 'paoloDR', 'ef5912829e6917cbb1bc345c45a5499cae38f229955367da575e49e53bd01f8a');  -- psw Paolo: paoloDR
    (1015, 'RZZDVD62H15F205N', 'Davide', 'Ruzzon', 'Via Zaghi, 25B', '31010', 'Borso', 'IT', 'davideR@gmail.com', '0423/910580', 'davide', '07d046d5fac12b3f82daf5035b9aae86db5adc8275ebfbf05ec83005a4a8ba3e');  -- psw Davide: david
    (1016, 'ZNTSFN72L15A757O', 'Stefano', 'Zanetti', 'Via S.Rocco, 13', '32100', 'Belluno', 'IT', 'stefanoZ@gmail.com', '0437/987654', 'stefano', '52518386cc33022de894fa0af047bd62666a63c2a6a6e86650e26955058c5acf');  -- psw Stefano: stefan
    -- da 14.10.21 in poi
    (1017, 'DNLBSS85L15C111W', 'Daniele', 'Basso', 'Via S.Pio X, 11b', '31017', 'Pieve del Grappa', 'IT', 'info@dabasso.com', '0423/777659', 'daniele', '3e360b6afa5191c974a280c9510c85a8663148feb6a9d5cad026c9a67365befa');  -- psw Daniele: giudabasso 
    (1018, 'MRCDGI72H15A703K', 'Diego', 'Marchiori', 'Via Melagrani, 72', '36061', 'Bassano del Grappa', 'IT', 'ritzdesigndj@gmail.com', '0424/887452', 'diego', '1158766d2940a87294dfca01cce3bafdabb542e02584d2211c28dc0af01e0723');  -- psw Diego: djritz  


-- Struttura della tabella Veicoli

CREATE TABLE veicoli (
    targa VARCHAR(10) NOT NULL,
    marca VARCHAR(15) NOT NULL,
    modello VARCHAR (20) NOT NULL,
    anno YEAR,
    motore VARCHAR(10) NOT NULL,
    potenza VARCHAR(7) NOT NULL,
    batteria VARCHAR(5) NOT NULL,
    cilindrata VARCHAR(5),
    proprietario_fk INT NOT NULL,                                 -- chiave esterna da utenti
    CONSTRAINT targaPk PRIMARY KEY (targa),
    CONSTRAINT codeFkV FOREIGN KEY (proprietario_fk) REFERENCES utenti (code)
    ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO veicoli (targa, marca, modello, anno, motore, potenza, batteria, proprietario_fk) VALUES
    ('RZ915NF', 'Evo', '3Electric', 2021, 'Elettrico', '116', '40', 1002),
    ('PN696AQ', 'Fiat', 'Nuova500', 2020, 'Elettrico', '95', '42', 1003),
    ('NY678XR', 'Hiunday', 'Ioniq', 2019, 'Elettrico', '109', '38', 1004),
    ('MK950CU', 'Renault', 'Zoe', 2021, 'Elettrico', '109', '52', 1004),
    ('QR997EW', 'Tesla', 'Model3', 2020, 'Elettrico', '351', '79', 1005);
    -- dopo 01.06.21
    ('LA091BW', 'Audi', 'Q4/50 e-tron', 2021, 'Elettrico', '220', '77', 1012); -- matteoF
    ('LA241CA', 'Citroen', 'e-C4 Feel', 2021, 'Elettrico', '57', '50', 1010); -- federicaM 
    ('LB169AB', 'Volkswagen', 'ID.4 City', 2021, 'Elettrico', '70', '52', 1009); -- marcoB
    ('LC215BA', 'Volvo', 'Xc40', 2021, 'Elettrico', '160', '78', 1013); -- emilioC
    ('LE421FU', 'Peugeot', 'e-Traveler stdAllure', 2021, 'Elettrico', '57', '75', 1014); -- PaoloDR
    ('LF163AU', 'BMW', 'iX xDrive50', 2021, 'Elettrico', '130', '112', 1015); -- davideR
    ('LF233GD', 'DS', 'DS3 Crossback E-T PL', 2021, 'Elettrico', '100', '50', 1016); -- stefanoZ
    -- da 14.10.21 in poi
    ('LG152AC', 'MERCEDES', 'EQV 300 Extralong', 2021, 'Elettrico', '150', '100', 1017); -- DanieleB  
    ('LH246GK', 'DS', 'DS3 Crossback E-T PL', 2021, 'Elettrico', '100', '50', 1018); -- DiegoM







CREATE TABLE bobine (
    id_bobina INT NOT NULL AUTO_INCREMENT,                    -- VsCode non riconose AUTO_INCREMENT
    tecnologia VARCHAR(8) NOT NULL,
    nome_bobina VARCHAR(10),                                  -- attributo forse inutile?? 
    zona VARCHAR(10) NOT NULL,
    prezzo FLOAT(5, 2),
    CONSTRAINT id_bobinaPk PRIMARY KEY (id_bobina)
);

INSERT INTO bobine (tecnologia, nome_bobina, zona, prezzo) VALUES
    ('Dinamica', 'FlowDk', 'D1sud', 0.45),
    ('Statica', 'FlowSk', 'S1nord', 0.31),
    ('Statica', 'FlowSk', 'S2est', 0.31),
    ('Dinamica', 'FlowDk', 'D1nord', 0.45);






CREATE TABLE ricariche (                                    -- deriva dall'associazione tra Veicolo e Bobina
    targa_fk VARCHAR(10) NOT NULL,                          -- chiave esterna da veicoli
    bobina_fk INT NOT NULL,                                 -- chiave esterna da bobine
    guidatore_fk INT,                                       -- chiave esterna da utenti
    data_in DATETIME,                                       -- tempo relativo al login: servono due attributi, inizio e fine?
    data_out DATETIME,
    carica FLOAT(5, 2),                                     -- VsCode non riconose il tipo Double, perche'? >> provare su DB!!!
    costo FLOAT(5, 2),
    pagato BOOLEAN,
    CONSTRAINT ricaricaPk PRIMARY KEY (targa_fk, bobina_fk, guidatore_fk, data_in),
    CONSTRAINT targaFkR FOREIGN KEY (targa_fk) REFERENCES veicoli (targa)
    ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT bobinaFk FOREIGN KEY (bobina_fk) REFERENCES bobine (id_bobina)
    ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT codeFkR FOREIGN KEY (guidatore_fk) REFERENCES utenti (code)
    ON DELETE CASCADE ON UPDATE CASCADE
);                                                                              

INSERT INTO ricariche (targa_fk, bobina_fk, guidatore_fk, data_in, data_out, carica, costo, pagato) VALUES
    ('PN696AQ', 2, 1003, '2021-05-08 12:19:17', '2021-05-08 16:26:10', 40, 12.40, 1),
    ('MK950CU', 1, 1004, '2021-05-09 06:13:25', '2021-05-09 08:51:09', 51, 24.30, 1),
    ('RZ915NF', 1, 1002, '2021-05-19 15:29:02', '2021-05-19 15:29:02', 38, 17.10, 1),
    ('RZ915NF', 2, 1002, '2021-05-20 12:18:09', '2021-05-20 16:18:09', 42, 13.02, 1),
    ('RZ915NF', 4, 1002, '2021-05-22 08:29:03', '2021-05-22 12:35:09', 43, 19.35, 1),
    ('RZ915NF', 4, 1002, '2021-05-23 09:32:15', '2021-05-23 13:14:11', 43, 19.35, 1),
    ('RZ915NF', 4, 1008, '2021-05-24 18:00:00', '2021-05-24 22:00:00', 43, 19.35, 1),
    ('RZ915NF', 4, 1009, '2021-05-24 18:30:00', '2021-05-24 22:50:00', 43, 19.35, 1),
    ('MK950CU', 1, 1009, '2021-05-09 06:14:25', '2021-05-09 09:51:09', 51, 24.30, 1),
    ('NY678XR', 1, 1009, '2021-05-23 11:36:58', '2021-05-23 16:25:24', 43, 19.35, 1),
    ('NY678XR', 4, 1009, '2021-05-24 12:36:54', '2021-05-24 17:25:21', 43, 19.35, 1),
    ('MK950CU', 1, 1009, '2021-05-26 05:23:16', '2021-05-26 10:43:14', 51, 24.30, 1),
    ('NY678XR', 4, 1009, '2021-05-26 11:17:54', '2021-05-24 15:15:27', 43, 19.35, 1),
    ('NY678XR', 4, 1010, '2021-05-26 12:11:53', '2021-05-24 16:11:28', 43, 19.35, 1),
    ('NY678XR', 4, 1010, '2021-05-26 18:09:55', '2021-05-24 22:10:29', 43, 19.35, 1),
    ('MK950CU', 4, 1010, '2021-05-26 20:23:14', '2021-05-26 23:13:11', 51, 24.30, 1),
    ('RZ915NF', 4, 1010, '2021-05-27 03:15:18', '2021-05-27 06:18:24', 43, 19.35, 1),
    ('RZ915NF', 1, 1002, '2021-05-28 15:23:21', '2021-05-28 19:23:21', 51, 24.30, 1);
    -- dopo 01.06.21


CREATE TABLE contocorrenti (
    cc VARCHAR(15) NOT NULL,
    iban VARCHAR(30),
    card_type VARCHAR(16),
    card_number VARCHAR(16),
    banca VARCHAR(30) NOT NULL,
    proprietario_fk INT,                                                        -- chiave esterna da utenti
    CONSTRAINT contoPk PRIMARY KEY (cc),                                        -- inserire pk anche su HP7
    CONSTRAINT codeFkC FOREIGN KEY (proprietario_fk) REFERENCES utenti (code)
    ON DELETE CASCADE ON UPDATE CASCADE
    );

INSERT INTO contocorrenti (cc, iban, card_type, card_number, banca, proprietario_fk) VALUES
    ('255226316344', 'IT31J0300203280255226316344', 'Mastercard', '5582578623486917', 'Unicredit', 1002),
    ('797592286993', 'IT28O0300203280797592286993', 'Visa', '4555746836227529', 'Intesa Sanpaolo', 1003),
    ('752344766963', 'IT18Q0300203280752344766963', 'Visa', '4096441607926915', 'Banca Sella', 1004),
    ('762954625449', 'IT51O0300203280762954625449', 'Amex', '375287997911532', 'Banco di Napoli', 1005),
    ('221814551184', 'IT64V0300203280221814551184', 'Mastercard', '5123623244614442', 'Deutsche Bank', 1006);
    -- nessun errore di inserimento dati,  fin qui!
    -- dopo 01.06.21
    ('707571543176', 'ES7401286736707571543176', 'Visa', '4637593344771057', 'Bankia', 1012);   -- matteoF
    ('317811326746', 'IT79O0300203280317811326746', 'Mastercard', '5481027280471525', 'Banco di Napoli', 1013);   -- emilioC
    ('334798311432', 'IT12K0300203280334798311432', 'Mastercard', '5283005139679761', 'Unicredit', 1014);   -- paoloDR
    ('617872397279', 'IT82D0300203280617872397279', 'Mastercard', '5209003064618160', 'Unicredit', 1015);   -- davideR
    ('191598873469', 'IT34M0300203280191598873469', 'Visa', '4125545301797144', 'BNL', 1016);   -- stefanoZ
    -- da 14.10.21 in poi
    ('347562355181', 'IT91E0300203280347562355181', 'Mastercard', '5209428381634394', 'BNL', 1017);   -- danieleB 
    ('158379392363', 'IT27D0300203280158379392363', 'Visa', '4519152621433726', 'Lloyds', 1018);   -- diegoM 