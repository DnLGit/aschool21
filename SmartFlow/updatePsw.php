<?php
    //  carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php";  
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato.


    if(!isset($_SESSION['username']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }
    $user=$_SESSION['username'];

    /*  se username presente nelle variabili di sessione è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<?php

    if(!isset($_POST["password0"]) || !isset($_POST["password1"])|| !isset($_POST["password2"]))
    {
        header("location:profilo.php?update=errorP");    // inserito errore tramite una "GET" nell'indirizzo url
        die();
    }

    $psw0 = $_POST["password0"];
    $psw1 = $_POST["password1"];
    $psw2 = $_POST["password2"];

    //  in futuro magari aggiungere altri controlli per validare input (es. nome utente almeno 3 caratteri)

    if($psw1 != $psw2)  // se le password non corrispondono invio errore GET su URL
    {
        header("location:profilo.php?update=differentP");
        die();
    }

    $psw_h0 = hash('sha256', $psw0);
    $psw_h1 = hash('sha256', $psw1);
   
 
    /*  SERVER SI CONNETTE AL DBMS:  dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente 'DnL' e pwd 'B32' al db smartflow
    */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
    
    
    /*  Query per controllo password attuale se uguale a quella nel db ... tramite poi l'if successivo   */     

    $query1 = "SELECT psw FROM utenti WHERE user = '$user'";
    $result1 = mysqli_query($conn, $query1);                    //  conn + query -> server richiede al dbms di eseguire $query
    $row1 = mysqli_fetch_assoc($result1);                       // mi ritorna i valori della select
    $psw_h = $row1['psw'];

    
    if($psw_h0 != $psw_h)       // se password attuale sbagliata invio una GET alla pagina "profilo" per l'alert
    {
        header("location:profilo.php?update=wrongP");
        die();
    }else                       /*  altrimenti continuo con la query2 per aggiornare la password dell'utente loggato)   */
    {
        $query2 = "UPDATE utenti SET psw ='$psw_h1' WHERE user ='$user'";

    }    

   /*   Uso mysqli_query per inviare la query al db, dove:
        il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) il comando che si vuole il dbms esegua
        in risposta true/false ad indicare l'esito dell'operazione
    */
    $result2 = mysqli_query($conn, $query2);                  //  conn + query -> server richiede dbms di eseguire $query
    
    if ($result2)                                            //  dbms risponde con TRUE se operazione andata a buon fine
    //echo $psw . "</br></br>";
    //echo $psw_h1;
        header("location: profilo.php?update=successP");    // inseriti messaggi tramite "GET" nell'indirizzo url: se arriva a "profilo" ci saranno i rispettivi alert!
        //  echo "UPDATE utenti SET psw ='$psw_h1' WHERE user ='$user' AND psw = '$psw_h0'";  // ATTENZIONE: ECHO COMMENTATA ==> EVENTUALMENTE PROVARE LA QUERY CHE NON VA!!!!
    else 
    {
        header("location: profilo.php?update=errorP");
    }
    

    /*  CHIUDO la connessione tra db e server (per questioni di memoria)    */ 
    mysqli_close($conn);
?>