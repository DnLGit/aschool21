<?php
    //carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php";  
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie sess_id arrivato


    if(!isset($_SESSION['username']))  //se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }
    $user=$_SESSION['username'];

    /*  se username presente nelle variabili di sessione, è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<?php

    if(!isset($_POST["password"]))
    {
        header("location:profilo.php?update=errorP");    // inserito eventuale errore tramite una "GET" nell'indirizzo url
        die();
    }

    $psw0 = $_POST["password"];      //  aggiungere in futuro altri controlli per controllare l'input: es. nome utente almeno 3 caratteri???
    

    $psw_h0 = hash('sha256', $psw0);
   
 
    /*  SERVER SI CONNETTE AL DBMS:  dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente '@@@' e pwd '###' al db smartflow
    */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)

   /*  Query per controllo password attuale se uguale a quella nel db ... tramite poi l'if successivo   */     

   $query = "SELECT psw FROM utenti WHERE user = '$user'";
   $result = mysqli_query($conn, $query);                    //  conn + query -> server richiede al dbms di eseguire $query
   $row = mysqli_fetch_assoc($result);                       // mi ritorna i valori della select
   $psw_h = $row['psw'];

   
   if($psw_h0 != $psw_h)       // se password attuale sbagliata invio una GET alla pagina P"profilo" per l'alert
   {
       header("location:profilo.php?update=wrongAc");
       die();
   }else                       /*  altrimenti continuo con la query per eliminare la tupla dell'utente loggato)   */
   {
        $query = "DELETE FROM utenti WHERE user ='$user' AND psw = '$psw_h'";
   }

  
    /*Uso mysqli_query per inviare la query al db, dove il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) è il comando che si vuole il dbms esegua in risposta true/false ad indicare l'esito dell'operazione.
    */
    $result = mysqli_query($conn, $query);              //  conn + query -> server richiede al dbms di eseguire $query
    
    if ($result){                                        //  dbms risponde con TRUE se operazione è andata a buon fine
        //header("location: profilo.php?update=successAc");
        header("location: logout.php?update=successAc");        
    }else 
    {
        header("location: profilo.php?update=errorAc");
    }

    /*  CHIUDO la connessione tra db e server (per questioni di memoria)    */ 
    mysqli_close($conn);
?>