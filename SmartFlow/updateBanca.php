<?php
    //  carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php";  
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato.


    if(!isset($_SESSION['username']) || !isset($_SESSION['code']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }
    $user = $_SESSION['username'];
    $codeFk = $_SESSION['code'];
    /*  se username presente nelle variabili di sessione è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<?php

    if(!isset($_POST["cc"]) || !isset($_POST["iban"])|| !isset($_POST["card_type"]) || !isset($_POST["card_number"]) || !isset($_POST["banca"]))
    {
        header("location:profilo.php?update=errorB");    // inserito errore tramite una "GET" nell'indirizzo url: se arriva a "profilo" ci sarà un alert!
        die();
    }

    /*  SERVER SI CONNETTE AL DBMS: si autentica con nome utente 'DnL' e pwd 'B32' al db smartflow  */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)

    $queryCCExist = "SELECT COUNT(*) AS num_cc FROM utenti, contocorrenti WHERE utenti.code=contocorrenti.proprietario_fk AND user ='$user'";

    $result = mysqli_query($conn, $queryCCExist);  //c onn + query -> server richiede al dbms di eseguire $query

    $row = mysqli_fetch_assoc($result); 
    
    $num_cc = intval($row['num_cc']); //intval = Integer.parseInt(string) di Java per trasformare la stringa in int

    //var_dump($num_cc);
    
    $cc = $_POST["cc"];
    $iban = $_POST["iban"];
    $card_type = $_POST["card_type"];
    $card_number = $_POST["card_number"];
    $banca = $_POST["banca"];
   
    $query = "";
    if($num_cc == 0)//conto corrente non esiste e quindi devi INSERIRE la riga
        $query = "INSERT INTO contocorrenti(cc, iban, card_type, card_number, banca, proprietario_fk) 
                    VALUES ('$cc', '$iban', '$card_type', '$card_number', '$banca', $codeFk)";
    
    else// conto corrente esiste già e quindi va modificata la riga
        $query = "UPDATE contocorrenti SET iban ='$iban', card_type ='$card_type', card_number ='$card_number', banca ='$banca' WHERE cc ='$cc' AND proprietario_fk=$codeFk"; //filtro con AND anche sul codice per motivi di sicurezza
    
    var_dump($query);

   /*  Uso mysqli_query per inviare la query al db, dove:
        il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) il comando che si vuole il dbms esegua
        in risposta true/false ad indicare l'esito dell'operazione
    */

    $result = mysqli_query($conn, $query);                  //  conn + query -> server richiede al dbms di eseguire $query
    
    if ($result)                                            //  dbms risponde con TRUE se operazione andata a buon fine
        header("location: profilo.php?update=successB");    // inseriti messaggi tramite "GET" nell'indirizzo url: se arriva a "profilo" ci saranno i rispettivi alert!
    else 
        header("location: profilo.php?update=errorB");
    
    /*  CHIUDO la connessione tra db e server (per questioni di memoria)    */ 
    mysqli_close($conn);
?>
