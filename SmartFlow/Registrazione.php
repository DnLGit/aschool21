<?php
    //carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php";

    if( !isset($_POST["username"]) || !isset($_POST["password1"]) || !isset($_POST["password2"]) ||
        !isset($_POST["name"]) || !isset($_POST["surname"]))
    {
        header("location:signin.php?error=register");
        die();
    }


    $user = $_POST["username"];
    $pwd1 = $_POST["password1"];
    $pwd2 = $_POST["password2"];
    $name = $_POST["name"];
    $surname = $_POST["surname"];
    

    //  da aggiungere in futuro altri controlli per validare input (es. nome utente almeno 3 caratteri)

    if($pwd1 != $pwd2)
    {   // password non corrispondono
        header("location:index.php?error=register&pwd=different");
        die();
    }

    $pwd_h = hash('sha256', $pwd1);
 
    /*  SERVER SI CONNETTE AL DBMS
        dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente '@@@' e pwd '###'
        server si connette al db 'smartflow'
    */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
    
    /*  Query da inviare al dbms perché la esegua (deve inserire la riga del nuovo utente che si sta registrando)   */
    $query = "INSERT INTO utenti(user, psw, nome, cognome) VALUES('$user', '$pwd_h', '$name', '$surname')";

    /*  Uso mysqli_query per inviare la query al db, dove:
        il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) il comando che si vuole il dbms esegua
        in risposta true/false ad indicare l'esito dell'operazione
    */

    $result = mysqli_query($conn, $query);  //  conn + query -> server richiede al dbms di eseguire $query
    
    if ($result)    //  dbms risponde con TRUE se l'operazione è andata a buon fine
    {
        /*  voglio redirezionare l'utente nell'area riservata per modificare i dati anagrafici
            quindi...creo la sessione, inserendo all'interno gli stessi dati che inserisco quando si logga, 
            ed anche gli stessi dati appena inseriti nella form di registrazione (user, nome, cognome)
        */
        session_start();
        $_SESSION['username'] = $user;
        $_SESSION['nome'] = $name;
        $_SESSION['cognome'] = $surname;    
        
        
        header("location:profilo.php?register=success");
    }else 
    {
        header("location:signin.php?error=register");
    }

    /*  CHIUDO la connessione tra db e server (per le solite questioni di occupazione memoria)    */ 
    mysqli_close($conn);
?>