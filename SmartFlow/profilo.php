<?php
    //carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php"; 
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato


    if(!isset($_SESSION['username']) || !isset($_SESSION['code']))  //se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }

    $code_utente = $_SESSION['code'];

    /*  se username presente nelle variabili di sessione è per forza la sua e quindi significa autenticazione corretta */
?>

<!DOCTYPE html> <!-- Attenzione: cercare qualche css in rete [Bootstrap???]  --> 
<html>
    <head>
        <title> profilo-SmartFlow </title>
            <?php 
                // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
                require "head.php"; 
            ?>
    </head>

    <body class="text-center mx-auto">          <!--   "style="text-align: center;"  class="text-center mx-auto"   -->
        <div>        <!-- class="text-center mx-auto" (bootstrap) -->     <!--   style="text-align: center;"  (html) -->
            <?php 
                if(isset($_GET['register']) && $_GET['register']=='success'){
                    echo "<p style='color: green'>REGISTRAZIONE AVVENUTA CON SUCCESSO</p>";
                    echo "<p <b>COMPILA SOTTO INSERENDO IL RESTO DEI DATI ANAGRAFICI</p>";                    
                }
            ?>
        <hr>
        </div>
            <div class="row">                       <!-- class="row" (bootstrap)   "text-center mx-auto"   -->
                <div class="col-2"><a href='index.php'><img src="images/SmartFlow Logo (rif01).jpg" class="img-fluid" alt="LogoSfW"></a></div>
                <div class="col-6"></div>
                <div class="col-2"><h5><a href='myaccount.php'>Home</a></h5></div>
                <div class="col-2"><h5><a href='signin.php'>Sign Out</a></h5></div>
            </div>
        <hr>


        <!-- ***********************  Parte Modifica Dati Standard (anagrafica) *************************************  -->


        <div>       <!-- class="text-center mx-auto" (bootstrap) -->     <!--   style="text-align: center;"  (html) -->
        <h4>il tuo Profilo </h4>
        </br>
        </div>
        <div class="row bg-dark text-light">                       <!-- class="row" (bootstrap) -->
                <?php
                    /*  Variabili inizialmente vuote da caricare con i dati fetchati dal db attraverso la SELECT sotto
                        
                        Nota: si potrebbe creare un array associativo al posto di tante variabili
                               $profilo = array("cf"=> NULL, "nome"=> NULL,...);
                    */ 
                    $cf = NULL;
                    $nome = NULL;
                    $cognome = NULL;
                    $via = NULL;
                    $cap = NULL;
                    $citta = NULL;
                    $nazione = NULL;
                    $email = NULL;
                    $telefono = NULL;

                    $cc = NULL;
                    $iban = NULL;
                    $card_type = NULL;
                    $card_number = NULL;
                    $banca = NULL;

                    $user = $_SESSION['username'];
                    
                    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
                    $query = "SELECT cf, nome, cognome, via, cap, citta, nazione, email, telefono
                    FROM utenti WHERE user = '$user'";

                    // la risposta alla chiamata mysqli_query, in caso venga fatta una select, contiene le righe selezionate 
                    $result = mysqli_query($conn, $query);  //  conn + query -> server richiede al dbms di eseguire $query
                
                    //  mysqli_num_rows(result) dove result è risultato della SELECT e ritorna il numero di righe passate (fetch)
                    if (mysqli_num_rows($result) == 1) 
                    {
                
                        //metti dentro a $row la riga ricevuta in risposta
                        $row = mysqli_fetch_assoc($result);
                        
                        $cf = $row['cf'];
                        $nome = $row['nome'];
                        $cognome = $row['cognome'];
                        $via = $row['via'];
                        $cap = $row['cap'];
                        $citta = $row['citta'];
                        $nazione = $row['nazione'];
                        $email = $row['email'];
                        $telefono = $row['telefono'];
                        
                    } 

                    $query = "SELECT cc, iban, card_type, card_number, banca 
                    FROM contocorrenti WHERE contocorrenti.proprietario_fk = $code_utente";

                    // la risposta alla chiamata mysqli_query, in caso venga fatta una select, contiene le righe selezionate 
                    $result = mysqli_query($conn, $query);  //  conn + query -> server richiede al dbms di eseguire $query
                
                    //  mysqli_num_rows(result) dove result è risultato della SELECT e ritorna il numero di righe passate (fetch)
                    if (mysqli_num_rows($result) == 1) 
                    {
                
                        //metti dentro a $row la riga ricevuta in risposta
                        $row = mysqli_fetch_assoc($result);

                        $cc = $row['cc'];
                        $iban = $row['iban'];
                        $card_type = $row['card_type'];
                        $card_number = $row['card_number'];
                        $banca = $row['banca'];

                        
                    } 
                    mysqli_close($conn);
                ?>
                
                <div class="col-1 "></div>

                <div class="col-3 text-start">
                <h5> Dati Attuali </h5><br/>
                    <p>Nome: <?=$nome?></p> 
                    <p>Cognome: <?=$cognome?></p>
                    <p>Codice Fiscale: <?=$cf?></p>  
                    <p>Via: <?=$via?></p> 
                    <p>CAP: <?=$cap?></p> 
                    <p>Città: <?=$citta?></p> 
                    <p>Nazione: <?=$nazione?></p> 
                    <p>Email: <?=$email?></p> 
                    <p>Telefono: <?=$telefono?></p></br></br></br></br>
                    
                    <p>Conto Corrente: <?=$cc?></p>
                    <p>Codice Iban: <?=$iban?></p>
                    <p>Carta di Credito: <?=$card_type?></p>
                    <p>Numero Carta: <?=$card_number?></p>
                    <p>Nome Banca: <?=$banca?></p>
                </div>


                <!-- ***********************  Parte Dati bancari *************************************  -->

                <div class="col 4 ">        <!-- class="text-center mx-auto" (bootstrap) -->
                </br></br>     
                    <h5> Dati Bancari </h5><br/>
                    <form method="POST" action="updateBanca.php">
                        
                        <!-- <div class="mb-3">
                            <label for="nomeA">Nome:</label> 
                            <input id="nomeA" type="text"  name="nome" placeholder="Nome" value="<?=$nome?>" required/>
                        </div> -->

                        Conto Corrente: 
                        <?php   if(strlen($cc)>0){
                                    echo $cc;
                                    echo "<br/><input type='hidden'  name='cc' placeholder='n°conto' value='$cc'/>";
                                }else
                                    echo "<br/><input type='text'  name='cc' placeholder='n°conto' value='$cc' required/>";
                        ?>
                        <br/><br/>
                        Iban: <br/><input type="text"  name="iban" placeholder="n°iban" value="<?=$iban?>" required/><br/><br/>
                        Carta di Credito: <br/><input type="text"  name="card_type" placeholder="nome carta" value="<?=$card_type?>" required/><br/><br/>
                        Numero Carta: <br/><input type="text"  name="card_number" placeholder="n°carta" value="<?=$card_number?>" required/><br/><br/>
                        Banca: <br/><input type="text"  name="banca" placeholder="nome Banca" value="<?=$banca?>" required/><br/><br/><br/><br/>  
                        

                        <input type="submit" name="submit" value="Aggiorna Dati"/>
                        <input type="reset" name="cancella" value="RESET"/>
                    </form><br/>

                    <?php 
                        if(isset($_GET['update'])){
                            if($_GET['update']=='successB'){
                                echo "<div class='alert alert-success'>DATI BANCARI AGGIORNATI</div>";
                            }

                            if($_GET['update']=='errorB'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: RIPROVA</div>";
                            }
                        }
                    ?>                 
                </div>
                
                <!-- ***********************  Parte Modifica Anagrafica *************************************  -->

                <div class="col-3 text-end">
                <h5> Modifica Anagrafica </h5><br/>
                    <form method="POST" action="updateAnagrafica.php">
                        
                        <!-- <div class="mb-3">
                            <label for="nomeA">Nome:</label> 
                            
                        </div> -->
                        Nome: <input id="nomeA" type="text"  name="nome" placeholder="Nome" value="<?=$nome?>" required/><br/>
                        Cognome: <input type="text"  name="cognome" placeholder="Cognome"  value="<?=$cognome?>" required/><br/><br/>
                        Codice Fiscale: <input type="text"  name="cf" placeholder="Codice Fiscale" value="<?=$cf?>" required/><br/><br/>
                        Via: <input type="text"  name="via" placeholder="Via" value="<?=$via?>" required/><br/>
                        Cap: <input type="text"  name="cap" placeholder="Cap" value="<?=$cap?>" required/><br/> 
                        Città: <input type="text"  name="citta" placeholder="Città" value="<?=$citta?>" required/><br/><br/> 
                        Nazione: <input type="text" name="nazione" placeholder="inserire sigla" value="<?=$nazione?>" required/><br/><br/>  
                        Email: <input type="email" name="email" placeholder="Email"  value="<?=$email?>"required/><br/>
                        Telefono: <input type="text" name="telefono" placeholder="cellulare" value="<?=$telefono?>" required/><br/><br/>

                        <input type="submit" name="submit" value="Aggiorna Dati"/>
                        <input type="reset" name="cancella" value="RESET"/>
                    </form><br/>

                    <?php 
                        if(isset($_GET['update'])){
                            if($_GET['update']=='successA'){
                                echo "<div class='alert alert-success'>UPDATE EFFETTUATO CON SUCCESSO</div>";
                            }

                            if($_GET['update']=='errorA'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: RIPROVA</div>";
                            }
                        }
                    ?>
                </div>
            <div class="col-1 "></div>
        </div>


        <!-- ***********************  Parte Modifica Dati Sensibili *************************************  -->


        <div class="text-center mx-auto">       <!-- class="text-center mx-auto" (bootstrap) -->     <!--   style="text-align: center;"  (html) -->
        </br></br><br/><h4> DATI SENSIBILI </h4>
                <h6></h6><br/>
        </div>
        <div class="row">                       <!-- class="row" (bootstrap) -->               

                <div class="col-1"></div>

                <div class="col-3">
                    <h5> Modifica password </h5><br/>
                    <form method="POST" action="updatePsw.php">

                        Password attuale: <input type="password" name="password0" placeholder="Password" required/><br/><br/>
                        Password nuova: <input type="password" name="password1" placeholder="Password" required/><br/><br/>   
                        Conferma nuova: <input type="password" name="password2" placeholder="Password" required/><br/><br/> 

                        <input type="submit" name="submit" value="Aggiorna Password"/>
                        <input type="reset" name="cancella" value="RESET"/>
                    </form><br/>

                    <?php 
                        if(isset($_GET['update'])){
                            if($_GET['update']=='successP'){
                                echo "<div class='alert alert-success'>UPDATE EFFETTUATO CON SUCCESSO</div>";
                            }

                            if($_GET['update']=='errorP'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: RIPROVA</div>";
                            }
                            if($_GET['update']=='wrongP'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: PASSWORD ATTUALE SBAGLIATA</div>";
                            }
                            if($_GET['update']=='differentP'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: PASSWORD NUOVE DIFFERENTI</div>";
                            }
                        }
                    ?>
                </div>

                <div class="col-2"></div>

                <div class="col-2"></div>

               
                <div class="col-3">
                    <h5> Cancella Account - [GDPR] </h5><br/>
                    <form method="POST" action="deleteAccount.php">

                        Inserisci Password: <input type="password" name="password" placeholder="Password" required/><br/><br/>
                        Elimina Account: <input type="submit" name="submit" value="Elimina"/>
                        <!-- Test <input type="button" onclick="alert('Sicuro???')" value="Click Me!"> -->                        
                        <!-- <input type="reset" name="cancella" value="ANNULLA"/> -->
                    </form><br/>

                    <?php 
                        if(isset($_GET['update'])){ // if commentato perché interviene quello nella pagina "index"!!!
                            if($_GET['update']=='successAc'){
                               // echo "<div class='alert alert-success'>ELIMINAZIONE EFFETTUATA CON SUCCESSO</div>";
                                //header("location: index.php?update=successAc");                                
                                
                            }
                            if($_GET['update']=='wrongAc'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: PASSWORD ATTUALE SBAGLIATA</div>";
                            }

                            if($_GET['update']=='errorAc'){
                                echo "<div class='alert alert-danger'>ERRORE NELL'UPDATE: RIPROVA</div>";
                            }
                        }
                    ?>
                </div>
                        
                </div>

                <div class="col-1"></div>
        </div>

        <hr/>

       <!--Footer START-->
        <footer>
            <div>
                <h4>
                    © 2021 Copyright: DnL@smartflow.energy
                </h3>
            </div>
        </footer>
        <!--Footer END-->
    </body>
</html>