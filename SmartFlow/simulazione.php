<?php
    //  carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php"; 
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato


    if(!isset($_SESSION['username']) || !isset($_SESSION['code']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }
    $user = $_SESSION['username'];
    $code_guidatore = $_SESSION['code'];
    /*  se username presente nelle variabili di sessione, è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<?php
 

    if(!isset($_POST["targa"]) || !isset($_POST["bobina"]) || !isset($_POST["durata"]) || !isset($_POST["pagato"]))
    {
        header("location:myaccount.php?update=errorS");    // inserito testo di errore tramite una "GET" nell'indirizzo url: se arriva a "profilo" ci sarà un alert!
        die();
    }

    $targa = $_POST["targa"];
    $id_bobina = $_POST["bobina"];
    $durata = $_POST["durata"];
    $pagato = $_POST["pagato"];
    $batt = NULL;
    $prezzo = NULL;
?>

<?php 
    //  Da implementare in futuro:
    /*  Prima di andare avanti, controlla se i dati anagrafici e di conto corrente presente  */
?>

<?php                   //   mi serve il valore della batteria per calcolare la ricarica in base al del tempo.
    
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);
    $user = $_SESSION['username'];
    
    //  recupero carica max del veicolo selezionato
    $queryBatteria = "  SELECT batteria 
                        FROM veicoli
                        WHERE veicoli.targa='$targa'";            
    
    $result = mysqli_query($conn, $queryBatteria);
    if (mysqli_num_rows($result) == 1){
        $row = mysqli_fetch_array($result);
        $batt = $row['batteria'];
    }
    
    //  recupero prezzo al kW della bobina selezionata
    $queryPrezzo = "SELECT prezzo FROM bobine WHERE id_bobina=$id_bobina";                              
    $result = mysqli_query($conn, $queryPrezzo);
    if (mysqli_num_rows($result) == 1){
        $row = mysqli_fetch_array($result);
        $prezzo=$row['prezzo'];
    }     
    
?>

<?php

    //$diff = abs($start_ts - $end_ts);             // differenza fra i due tempi: ho il tempo di ricarica.
    $diff = $durata * 60; //durata in min per 60 così da aver il valore in secondi
    $timeFull=7200;                         // Considerate 2 ore per un pieno di una batteria di 40watt (è tutto in evoluzione, si parla anche di pochi minuti ...)
    $watt= ($batt*$diff)/$timeFull;

    $carica =  $watt;
    if($carica>$batt)   //  la carica non può sforare il valore della batteria
        $carica=$batt;
    

    $costo=$carica*$prezzo;
    
    echo "Tempo di ricarica: $diff <br/>";
    echo "Potenza di Ricarica: $carica <br/>";
    echo "Totale costo Ricarica: $costo <br/>";
    echo "Prezzo bobina: $prezzo <br/><br/>";

    echo "id_bobina: $id_bobina <br/><br/>";
    echo "guidatore_fk: $code_guidatore <br/><br/>";
    

    /* var_dump($_POST['start']);
    var_dump($_POST['end']);
 */
?>    

<?php
   
    /*  SERVER SI CONNETTE AL DBMS:  dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente 'DnL' e pwd 'B32' al db smartflow
    */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!) 

    /* https://www.w3schools.com/mysql/func_mysql_date_add.asp */ 
    $query = "INSERT INTO ricariche (targa_fk, bobina_fk, guidatore_fk, data_in, data_out, carica, costo, pagato) VALUES 
            ('$targa', '$id_bobina', '$code_guidatore', NOW(), DATE_ADD(NOW(), INTERVAL $durata MINUTE), '$carica', '$costo', '$pagato')";

   /*  Uso mysqli_query per inviare la query al db, dove:
        il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) il comando che si vuole il dbms esegua
        in risposta true/false ad indicare l'esito dell'operazione
    */

    $result = mysqli_query($conn, $query);                  //  conn + query -> server richiede al dbms di eseguire $query
    
    if ($result)                                            //  dbms risponde con TRUE se operazione andata a buon fine
        header("location: myaccount.php?update=successS");    // inseriti messaggi tramite "GET" nell'indirizzo url: se arriva a "profilo" ci saranno i rispettivi alert!
    else 
    {
        header("location: myaccount.php?update=errorS");
    }

    /*  CHIUDO la connessione tra db e server (per questioni di memoria)    */ 
    mysqli_close($conn);
?>