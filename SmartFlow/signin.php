<!DOCTYPE html>
<html>
    <head><title> signIn-SmartFlow </title>
        <?php 
            // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
            require "head.php"; 
        ?>
    </head>
    <body style="text-align: center;">                                  <!--   style="text-align: center;"  -->
    <hr>
        <div class="row ">                       <!-- class="row" (bootstrap)   "text-center mx-auto"   -->
            <div class="col-2"><a href='index.php'><img src="images/SmartFlow Logo (rif01).jpg" class="img-fluid" alt="LogoSfW"></a></div>
            <div class="col-2"><h5><a href='mission.php'>Mission</a></h5></div>
            <div class="col-2"><h5><a href='tecnologia.php'>Technology</a></h5></div>
            <div class="col-2"><h5><a href='info.php'>Info</a></h5></div>
            <div class="col-2"></div>
            <div class="col-2"></div>
        </div>
        <hr>
        
        <!-- ***********************  Accedi:  Log-In di accesso utente già registrato  *************************************  -->
    
        <div class="row bg-dark text-light">
        <div class="col-2"></div>
            <div class="col-4">                                             <!--   style="text-align: center;"  -->
                <br/>
                <h1> Accedi </h1>
                <br/><br/>
                <form method="POST" action="accedi.php"> 
                    Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>        
                    Password: <input type="password" name="password" placeholder="Password" required/><br/><br/>  
                    
                    <input type="submit" name="submit" value="Log In"/>
                    <input type="reset" name="cancella" value="Cancella Tutto"/>
                </form>
                <?php 
                    if(isset($_GET['error']) && $_GET['error']=='login')
                        echo "<p style='color: red'>ERRORE IN LOGIN</p>";
                ?>
            </div>

            <!-- ***********************  Registrazione:  per nuovi utenti *************************************  -->
            
            
            <div class="col-4">                  <!-- style="margin-top: 5%;"    Attenzione: cercare qualche css in rete [Bootstrap???]  -->
                <br/>
                <h1> Registrati</h1>
                <br/><br/>
                <form method="POST" action="registrazione.php">
                    Nome: <input type="text"  name="name" placeholder="Nome" required/><br/><br/>
                    Cognome: <input type="text"  name="surname" placeholder="Cognome" required/><br/><br/>
                    Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>  
                    Password: <input type="password" name="password1" placeholder="Password" required/><br/><br/>  
                    Conferma Password: <input type="password" name="password2" placeholder="Password" required/><br/><br/>  
                    
                
                    <input type="submit" name="submit" value="Crea il tuo Account"/>
                    <input type="reset" name="cancella" value="Cancella Tutto"/>

                    <?php


                        if(isset($_GET['error']) && $_GET['error']=='register'){    // se gli arrivano gli avvisi come GET nell'URL stampa gli echo                             
                            
                            echo "<p style='color: red'>ERRORE IN REGISTRAZIONE</p>";                        

                            if(isset($_GET['pwd']) && $_GET['pwd']=='different')
                                echo "<p style='color: red'>Le password inserite non corrispondono</p>";
                        }
                    ?><br/><br/>
                </form>
            <div class="col-2"></div>
        </div>
    </body>
</html>