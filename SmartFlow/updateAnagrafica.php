<?php
    //  carico tramite "require_once" variabili dal file "credentials.php" per accedere al db (vedi sotto $conn)
    require_once "credentials.php"; 
    
    session_start();    //  carico i dati sull'array $_SESSION sulla base del cookie arrivato


    if(!isset($_SESSION['username']))  //   se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }
    $user=$_SESSION['username'];

    /*  se username presente nelle variabili di sessione, è per forza la sua e quindi significa che si è autenticato correttamente */
?>

<?php

    if(!isset($_POST["cf"]) || !isset($_POST["nome"])|| !isset($_POST["cognome"]) || !isset($_POST["via"]) || !isset($_POST["cap"]) || !isset($_POST["citta"]) || !isset($_POST["nazione"]) || !isset($_POST["email"]) || !isset($_POST["telefono"]))
    {
        header("location:profilo.php?update=errorA");    // inserito errore tramite una "GET" nell'indirizzo url: se arriva a "profilo" ci sarà un alert!
        die();
    }


    $cf = $_POST["cf"];
    $nome = $_POST["nome"];
    $cognome = $_POST["cognome"];
    $via = $_POST["via"];
    $cap = $_POST["cap"];
    $citta = $_POST["citta"];
    $naz = $_POST["nazione"];
    $email = $_POST["email"];
    $tel = $_POST["telefono"];
 
    /*  SERVER SI CONNETTE AL DBMS:  dbms si trova all'url 'localhost' (indica il dominio del dbms)
        server si autentica con nome utente '@@@' e pwd '###' al db smartflow
    */
    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);   //   conn server to dbms (anche qui ricordarsi file "credentials.php per l'accesso => ok inserito in alto!!)
    
    /*  Query da inviare al dbms perché la esegua (deve aggiornare la riga dell'utente loggato)   */
    /*  Usare UPDATE!!! LA RIGA dell'utente esiste già, si sta solo aggiornando dei campi    */     

    $query = "UPDATE utenti SET cf ='$cf', nome ='$nome', cognome ='$cognome', via ='$via', cap ='$cap', citta ='$citta', nazione ='$naz', email ='$email', telefono ='$tel' WHERE user ='$user'";

   /*  Uso mysqli_query per inviare la query al db, dove:
        il primo parametro (qui $conn) della chiamata è una connessione già creata 
        e il secondo parametro (qui $query) il comando che si vuole il dbms esegua
        in risposta true/false ad indicare l'esito dell'operazione
    */

    $result = mysqli_query($conn, $query);                  //  conn + query -> server richiede al dbms di eseguire $query
    
    if ($result)                                            //  dbms risponde con TRUE se operazione andata a buon fine
        header("location: profilo.php?update=successA");    // inseriti messaggi tramite "GET" nell'indirizzo url: se arriva a "profilo" ci saranno i rispettivi alert!
    else 
    {
        header("location: profilo.php?update=errorA");
    }

    /*  CHIUDO la connessione tra db e server (per questioni di memoria)    */ 
    mysqli_close($conn);
?>