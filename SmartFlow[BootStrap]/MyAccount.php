<?php 
    //carico variabili per accedere al db
    require_once "credentials.php";

    session_start();    //carico i dati sull'array $_SESSION sulla base del cookie sess_id arrivato


    if(!isset($_SESSION['username']))  //se username non presente nelle variabili di sessione -> utente NON autenticato
    {
        echo "Area riservata <br/> <a href='index.php'>Torna indietro</a>";
        die();
    }  

    /*se username presente nelle variabili di sessione, 
    è per forza la sua (non si discute!) e quindi significa che si è autenticato correttamente */
?>
<!DOCTYPE html>
<html>
    <head><title> MyAccount-SmartFlow </title> 
        <?php 
            // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
            require "head.php"; 
        ?>
    </head>

    <body style="text-align: center">
        <h1>MyAccount</h1>                                                       <!--   style="text-align: center;"  -->
            <div>
                Ciao <?=$_SESSION['username']?>
                
                <?=$_SESSION['nome']." ".$_SESSION['cognome']?>
                <br/> <br/>
                <hr/>
                
                <?php
                    $conn = mysqli_connect($host, $usernameDB, $pwdDB, $nameDB);//conn server to dbms usando le variabili da credentials.php
                    $user = $_SESSION['username'];
                    $query = " SELECT targa_fk, data_in, data_out, carica  FROM utenti, ricariche 
                    WHERE utenti.code = ricariche.guidatore_fk AND utenti.user = '$user'";
                    
                    $result = mysqli_query($conn, $query);
                    if (mysqli_num_rows($result) > 0) {
                        echo "<table class='table w-50 mx-auto'>";
                        echo "<thead>
                                <tr>
                                    <th>Targa</th>
                                    <th>Arrivo</th>
                                    <th>Uscita</th>
                                    <th>Ricarica kW</th>
                                    <th>pagato</th>
                                </tr>
                            </thead>";
                        
                        echo "<tbody>"; 
                        while ($row = mysqli_fetch_array($result)) {
                            echo "<tr>";
                                echo "<td>".$row['targa_fk']."</td>";
                                echo "<td>".$row['data_in']."</td>";
                                echo "<td>".$row['data_out']."</td>";
                                echo "<td>".$row['carica']."</td>";
                                echo "<td>?</td>";
                            echo "</tr>";
                        }
                        echo "</tbody>"; 
                        echo "</table>";
                    }else{

                        echo "  <!--ALERT BOOTSTRAP-->
                                <div class='alert alert-warning text-center fw-bold w-50 mx-auto' role='alert'>
                                    <h4>Devi ancora effettuare una ricarica con i nostri sistemi!</h4>
                                </div>";
                    }
                                            
                    /*
                        Attenzione:
                            scrivere <?php echo $_SESSION['n']; ?>
                            oppure   <?=$_SESSION['n']?>
                        è la stessa cosa!!
                    */
                ?>      

            </div>
        <hr/>
        <a href="logout.php">Logout</a>
    </body>
</html>