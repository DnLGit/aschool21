<!DOCTYPE html>
<html>    
    <head>
    <title> accedi-SmartFlow </title>

        <?php 
            // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
            require "head.php"; 
        ?>
    </head>

    <body>
        <div class="text-center mx-auto">                                             <!--   style="text-align: center;"  -->
            <h1> Accedi</h1>
            <hr><br/>
            <form method="POST" action="accedi.php"> 
                Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>        
                Password: <input type="password" name="password" placeholder="Password" required/><br/><br/>  
                
                <input type="submit" name="submit" value="Log In"/>
                <input type="reset" name="cancella" value="Cancella Tutto"/>
            </form>
            <?php 
                if(isset($_GET['error']) && $_GET['error']=='login')
                    echo "<p style='color: red'>ERRORE IN LOGIN</p>";
            ?>
        </div>
        <hr/>
        <div class="text-center mx-auto mt-5">
            <h1> Registrati</h1>
            <hr><br/>
            <form method="POST" action="registrazione.php">
                Nome: <input type="text"  name="name" placeholder="Nome" required/><br/><br/>
                Cognome: <input type="text"  name="surname" placeholder="Cognome" required/><br/><br/>
                Username: <input type="text"  name="username" placeholder="username" required/><br/><br/>  
                Password: <input type="password" name="password1" placeholder="Password" required/><br/><br/>  
                Conferma Password: <input type="password" name="password2" placeholder="Password" required/><br/><br/>  
                
               
                <input type="submit" name="submit" value="Crea il tuo Account"/>
                <input type="reset" name="cancella" value="Cancella Tutto"/>

                <?php
                     
                    if(isset($_GET['register']) && $_GET['register']=='success'){
                        echo "<p style='color: green'>REGISTRAZIONE AVVENUTA CON SUCCESSO</p>";
                    }


                    if(isset($_GET['error']) && $_GET['error']=='register'){                                        
                        
                        echo "<p style='color: red'>ERRORE IN REGISTRAZIONE</p>";

                        if(isset($_GET['pwd']) && $_GET['pwd']=='different')
                            echo "<p style='color: red'>Le password inserite non corrispondono</p>";
                    }
                ?>
            </form>
        </div>
    </body>
</html>