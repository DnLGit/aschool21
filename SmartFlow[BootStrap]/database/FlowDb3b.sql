CREATE DATABASE SmartFlow3;
USE SmartFlow3;

-- Struttura della tabella Veicoli

CREATE TABLE Veicoli (
    Targa VARCHAR(10) NOT NULL,
    Marca VARCHAR(15) NOT NULL,
    Modello VARCHAR (20) NOT NULL,
    Anno YEAR,
    Motore VARCHAR(10) NOT NULL,
    Potenza VARCHAR(7) NOT NULL,
    Batteria VARCHAR(5) NOT NULL,
    Cilindrata VARCHAR(5),
    CONSTRAINT targaPk PRIMARY KEY (targa)
);

INSERT INTO Veicoli (Targa, Marca, Modello, Anno, Motore, Potenza, Batteria) VALUES
    ('RZ915NF', 'Evo', '3Electric', 2021, 'Elettrico', '85/116', '40'),
    ('PN696AQ', 'Fiat', 'Nuova500', 2020, 'Elettrico', '70/95', '23/42'),
    ('NY678XR', 'Hiunday', 'Ioniq', 2019, 'Elettrico', '80/109', '0/38'),
    ('MK950CU', 'Renault', 'Zoe', 2021, 'Elettrico', '80/109', '52'),
    ('QR997EW', 'Tesla', 'Model3', 2020, 'Elettrico', '258/351', '53/79');      



CREATE TABLE Utenti (
    Code VARCHAR(10) NOT NULL,                         -- meglio questo come pk??
    Cf VARCHAR(16) NOT NULL,
    Nome VARCHAR(10) NOT NULL,
    Cognome VARCHAR(20) NOT NULL,
    Via VARCHAR(30),
    Cap VARCHAR(8),                                      -- 8 = max Brasile
    Citta VARCHAR(30),
    Nazione VARCHAR(2),
    Email VARCHAR(50),                                  -- leggo che si potrebbe usare "NVARCHAR" ...!!
    Bank VARCHAR(30) NOT NULL,                          -- cc, n° carta o Iban ...
    Pagato BOOLEAN,
    User VARCHAR(16) NOT NULL,                          -- il prof. aggiunge user e psw di accesso al database.
    Psw VARCHAR(256) NOT NULL,                          -- dovrebbe essere criptata, non in chiaro!
    CONSTRAINT codePk PRIMARY KEY (Code)
);

INSERT INTO Utenti (Code, Cf, Nome, Cognome, Via, Cap, Citta, Nazione, Email, Bank, Pagato, User, Psw) VALUES
    ('SF001VC', 'CLMVLR19E08E086L', 'Valerio', 'Calamaro', 'R.Carrieri, 98', '84065', 'Piaggine', 'IT', 'valerio.calamaro@virgilio.it', '5209894517896761', 1, 'Valerio01', 'GsIgANE6F8y5#10'),
    ('SF002NS', 'SRTNBR78L31D719R', 'Norberto', 'Sartelli', 'A.Buttie, 25', '83040', 'Cairano', 'IT', 'norberto.sartelli@katamail.it', '4117862551508240', 1, 'Norberto02', '!9aV5AfMWrOLS6q'),
    ('SF003RL', 'LNNRNI50M18F226E', 'Rino', 'Lanni', 'R.Fulton, 137', '25071', 'Agnosine', 'IT', 'rino.lanni@tiscali.it', '5127390717675260', 1, 'Rino03',  'zp&E0@Bw38vL7E5'),
    ('SF004AM', 'MRCNTA51S45F698C', 'Anita', 'Marchesano', 'A.Mapelli, 96', '83040', 'Candida', 'IT', 'anita.marchesano@aruba.it', '4188508968378263', 1, 'Anita04',  '@jC2EU6YiD96wFD'),
    ('SF005NR', 'RSGNLN71A55L959K', 'Natalina', 'Resegotti', 'P.Mascagni, 29', '93010', 'Montedoro', 'IT', 'natalina.resegotti@yahoo.com', '4557357958224191', 1, 'Natalina05', 'r08NhcppBshDR!n');       





CREATE TABLE Bobine (
    IdCoil INT NOT NULL AUTO_INCREMENT,                 -- VsCode non riconose AUTO_INCREMENT
    Tecnologia VARCHAR(8) NOT NULL,
    NomeC VARCHAR(10),                                  -- attributo forse inutile?? 
    Zona VARCHAR(10) NOT NULL,
    Prezzo FLOAT(5, 2),
    CONSTRAINT idcoilPk PRIMARY KEY (IdCoil)
);

INSERT INTO Bobine (Tecnologia, NomeC, Zona, Prezzo) VALUES
    ('Dinamica', 'FlowDk', 'D1sud', 0.45),
    ('Statica', 'FlowSk', 'S1nord', 0.31),
    ('Statica', 'FlowSk', 'S2est', 0.31),
    ('Dinamica', 'FlowDk', 'D1nord', 0.45);






CREATE TABLE Ricariche (                                -- deriva dall'associazione tra Veicolo e Bobina
    targaFk VARCHAR(10) NOT NULL,                       -- chiave esterna da veicoli
    idcoilFk INT NOT NULL,                              -- chiave esterna da bobine
    codeFk VARCHAR(10),                                 -- chiave esterna da utenti
    DataIn DATETIME,                                    -- tempo relativo al login: servono due attributi, inizio e fine?
    DataOut DATETIME,
    Carica FLOAT(5, 2),                                 -- VsCode non riconose il tipo Double, perche'? >> provare su DB!!!
    Costo FLOAT(5, 2),
    CONSTRAINT ricaricaPk PRIMARY KEY (targaFk, idcoilFk),
    CONSTRAINT targaFk FOREIGN KEY (targaFk) REFERENCES Veicoli (Targa),
    CONSTRAINT idcoilFk FOREIGN KEY (idcoilFk) REFERENCES Bobine (IdCoil),
    CONSTRAINT codeFk FOREIGN KEY (codeFk) REFERENCES Utenti (Code)
);                                                                              -- nessun errore inserendo fin qui!


