-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 09, 2021 alle 09:08
-- Versione del server: 10.4.17-MariaDB
-- Versione PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartflow4`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `bobine`
--

CREATE TABLE `bobine` (
  `IdCoil` int(11) NOT NULL,
  `Tecnologia` varchar(8) NOT NULL,
  `NomeC` varchar(10) DEFAULT NULL,
  `Zona` varchar(10) NOT NULL,
  `Prezzo` float(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bobine`
--

INSERT INTO `bobine` (`IdCoil`, `Tecnologia`, `NomeC`, `Zona`, `Prezzo`) VALUES
(1, 'Dinamica', 'FlowDk', 'D1sud', 0.45),
(2, 'Statica', 'FlowSk', 'S1nord', 0.31),
(3, 'Statica', 'FlowSk', 'S2est', 0.31),
(4, 'Dinamica', 'FlowDk', 'D1nord', 0.45);

-- --------------------------------------------------------

--
-- Struttura della tabella `pagamenti`
--

CREATE TABLE `pagamenti` (
  `Cc` varchar(15) NOT NULL,
  `Iban` varchar(30) DEFAULT NULL,
  `CrCard` varchar(16) DEFAULT NULL,
  `Banca` varchar(30) NOT NULL,
  `Pagato` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `pagamenti`
--

INSERT INTO `pagamenti` (`Cc`, `Iban`, `CrCard`, `Banca`, `Pagato`) VALUES
('255226316344', 'IT31J0300203280255226316344', '5209894517896761', 'Unicredit', 1),
('797592286993', 'IT28O0300203280797592286993', '4117862551508240', 'Intesa Sanpaolo', 1),
('752344766963', 'IT18Q0300203280752344766963', '5127390717675260', 'Banca Sella', 1),
('762954625449', 'IT51O0300203280762954625449', '4188508968378263', 'Banco di Napoli', 1),
('221814551184', 'IT64V0300203280221814551184', '4557357958224191', 'Deutsche Bank', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ricariche`
--

CREATE TABLE `ricariche` (
  `targaFk` varchar(10) NOT NULL,
  `idcoilFk` int(11) NOT NULL,
  `codeFk` varchar(10) DEFAULT NULL,
  `DataIn` datetime DEFAULT NULL,
  `DataOut` datetime DEFAULT NULL,
  `Carica` float(5,2) DEFAULT NULL,
  `Costo` float(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `Code` varchar(10) NOT NULL,
  `Cf` varchar(16) NOT NULL,
  `Nome` varchar(10) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Via` varchar(30) DEFAULT NULL,
  `Cap` varchar(8) DEFAULT NULL,
  `Citta` varchar(30) DEFAULT NULL,
  `Nazione` varchar(2) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `User` varchar(16) NOT NULL,
  `Psw` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`Code`, `Cf`, `Nome`, `Cognome`, `Via`, `Cap`, `Citta`, `Nazione`, `Email`, `User`, `Psw`) VALUES
('SF001VC', 'CLMVLR19E08E086L', 'Valerio', 'Calamaro', 'R.Carrieri, 98', '84065', 'Piaggine', 'IT', 'valerio.calamaro@virgilio.it', 'Valerio01', 'GsIgANE6F8y5#10'),
('SF002NS', 'SRTNBR78L31D719R', 'Norberto', 'Sartelli', 'A.Buttie, 25', '83040', 'Cairano', 'IT', 'norberto.sartelli@katamail.it', 'Norberto02', '!9aV5AfMWrOLS6q'),
('SF003RL', 'LNNRNI50M18F226E', 'Rino', 'Lanni', 'R.Fulton, 137', '25071', 'Agnosine', 'IT', 'rino.lanni@tiscali.it', 'Rino03', 'zp&E0@Bw38vL7E5'),
('SF004AM', 'MRCNTA51S45F698C', 'Anita', 'Marchesano', 'A.Mapelli, 96', '83040', 'Candida', 'IT', 'anita.marchesano@aruba.it', 'Anita04', '@jC2EU6YiD96wFD'),
('SF005NR', 'RSGNLN71A55L959K', 'Natalina', 'Resegotti', 'P.Mascagni, 29', '93010', 'Montedoro', 'IT', 'natalina.resegotti@yahoo.com', 'Natalina05', 'r08NhcppBshDR!n');

-- --------------------------------------------------------

--
-- Struttura della tabella `veicoli`
--

CREATE TABLE `veicoli` (
  `Targa` varchar(10) NOT NULL,
  `Marca` varchar(15) NOT NULL,
  `Modello` varchar(20) NOT NULL,
  `Anno` year(4) DEFAULT NULL,
  `Motore` varchar(10) NOT NULL,
  `Potenza` varchar(7) NOT NULL,
  `Batteria` varchar(5) NOT NULL,
  `Cilindrata` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `veicoli`
--

INSERT INTO `veicoli` (`Targa`, `Marca`, `Modello`, `Anno`, `Motore`, `Potenza`, `Batteria`, `Cilindrata`) VALUES
('MK950CU', 'Renault', 'Zoe', 2021, 'Elettrico', '80/109', '52', NULL),
('NY678XR', 'Hiunday', 'Ioniq', 2019, 'Elettrico', '80/109', '0/38', NULL),
('PN696AQ', 'Fiat', 'Nuova500', 2020, 'Elettrico', '70/95', '23/42', NULL),
('QR997EW', 'Tesla', 'Model3', 2020, 'Elettrico', '258/351', '53/79', NULL),
('RZ915NF', 'Evo', '3Electric', 2021, 'Elettrico', '85/116', '40', NULL);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `bobine`
--
ALTER TABLE `bobine`
  ADD PRIMARY KEY (`IdCoil`);

--
-- Indici per le tabelle `ricariche`
--
ALTER TABLE `ricariche`
  ADD PRIMARY KEY (`targaFk`,`idcoilFk`),
  ADD KEY `idcoilFk` (`idcoilFk`),
  ADD KEY `codeFk` (`codeFk`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`Code`);

--
-- Indici per le tabelle `veicoli`
--
ALTER TABLE `veicoli`
  ADD PRIMARY KEY (`Targa`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `bobine`
--
ALTER TABLE `bobine`
  MODIFY `IdCoil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ricariche`
--
ALTER TABLE `ricariche`
  ADD CONSTRAINT `codeFk` FOREIGN KEY (`codeFk`) REFERENCES `utenti` (`Code`),
  ADD CONSTRAINT `idcoilFk` FOREIGN KEY (`idcoilFk`) REFERENCES `bobine` (`IdCoil`),
  ADD CONSTRAINT `targaFk` FOREIGN KEY (`targaFk`) REFERENCES `veicoli` (`Targa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
