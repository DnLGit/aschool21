
    <!-- file "head.php" cumulativo che sarà collegato a tutte le pagine -->
    
    <!--  <title> head-SmartFlow </title>         tolto e commentato per avere "title" diversi per ogni pagina -->
    <meta charset="UTF-8"/>
    <link rel="icon" href="images/icon/LogoFlowCharge.ico"/>
    
    <!--meta-tag per rendere il sito responsive a diverse dimensioni per gli schermi-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    
    <!-- CSS bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper for Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="css/FlowStyle.css"/>

