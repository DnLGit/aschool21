<!DOCTYPE html>
<html>
    <head>
        <title> logo-SmartFlow </title> 
            <?php 
                // Include nel tag "head" della pagina HTML Link a file "css", file "icon" e altri metadati (UTF-8, vieport ... )
                require "head.php"; 
            ?>
            <meta charset="utf-8">
    </head>

    <body>
        
        <!--Navbar bootstrap START-->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand text-light fw-bold" href="#">
                    <img class="logo" src="images/icon/LogoFlowCharge.png" alt="LogoFC" />
                    Ricarica Veicoli Elettrici
                </a>
            </div>
        </nav>
        <!--Navbar bootstrap END-->

        <hr class="text-white mt-5"/>

        <div class="mt-5 container-fluid text-center mx-auto">                            
            <h1>  Ricarica Veicoli Elettrici </h1>                      <!--   style="text-align: center;"  -->
            <hr>

            <h2><a href='index.php'> Entra </a></h2>

            <img class="img-fluid" src="images/wave-64170_1920.jpg">      <!-- ricordarsi di bloccare la barra in alto -->
            
        
        </div>

        <hr/>
        
        <!--Footer START-->
        <footer id="footer" class="bg-dark text-light fw-bold">
            <div class="text-center footer">
                <h4>
                    © 2021 Copyright: DnL
                </h3>
            </div>
        </footer>
        <!--Footer END-->
    </body>
</html>